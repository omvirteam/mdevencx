<?php
include "./etc/om_config.inc";
$currentExchange = "CX";
if(!isset($_SESSION['toDate'])) 
{
  header("Location: selectDtSession.php?goTo=clientTrades");
}
else
{
  include "./etc/functions.inc";
  $smarty = new SmartyWWW();
  
  $message = "";
  
///////////////////////////Date 1 to 7 :Start
if(!isset($_SESSION['fromDateDate1to7']))
{
	$_SESSION['fromDateDate1to7'] = $_SESSION['fromDate'];
}
$dateToUse = $_SESSION['fromDateDate1to7'];

$date1day = date("d", mktime(0, 0, 0, substr($dateToUse,5,2)   , substr($dateToUse,8,2), substr($dateToUse,0,4)));
$date1month = date("m", mktime(0, 0, 0, substr($dateToUse,5,2) , substr($dateToUse,8,2), substr($dateToUse,0,4)));
$date1year = date("Y", mktime(0, 0, 0, substr($dateToUse,5,2)  , substr($dateToUse,8,2), substr($dateToUse,0,4)));

$date2day = date("d", mktime(0, 0, 0, substr($dateToUse,5,2)   , substr($dateToUse,8,2)+1, substr($dateToUse,0,4)));
$date2month = date("m", mktime(0, 0, 0, substr($dateToUse,5,2) , substr($dateToUse,8,2)+1, substr($dateToUse,0,4)));
$date2year = date("Y", mktime(0, 0, 0, substr($dateToUse,5,2)  , substr($dateToUse,8,2)+1, substr($dateToUse,0,4)));

$date3day = date("d", mktime(0, 0, 0, substr($dateToUse,5,2)   , substr($dateToUse,8,2)+2, substr($dateToUse,0,4)));
$date3month = date("m", mktime(0, 0, 0, substr($dateToUse,5,2) , substr($dateToUse,8,2)+2, substr($dateToUse,0,4)));
$date3year = date("Y", mktime(0, 0, 0, substr($dateToUse,5,2)  , substr($dateToUse,8,2)+2, substr($dateToUse,0,4)));

$date4day = date("d", mktime(0, 0, 0, substr($dateToUse,5,2)   , substr($dateToUse,8,2)+3, substr($dateToUse,0,4)));
$date4month = date("m", mktime(0, 0, 0, substr($dateToUse,5,2) , substr($dateToUse,8,2)+3, substr($dateToUse,0,4)));
$date4year = date("Y", mktime(0, 0, 0, substr($dateToUse,5,2)  , substr($dateToUse,8,2)+3, substr($dateToUse,0,4)));

$date5day = date("d", mktime(0, 0, 0, substr($dateToUse,5,2)   , substr($dateToUse,8,2)+4, substr($dateToUse,0,4)));
$date5month = date("m", mktime(0, 0, 0, substr($dateToUse,5,2) , substr($dateToUse,8,2)+4, substr($dateToUse,0,4)));
$date5year = date("Y", mktime(0, 0, 0, substr($dateToUse,5,2)  , substr($dateToUse,8,2)+4, substr($dateToUse,0,4)));

$date6day = date("d", mktime(0, 0, 0, substr($dateToUse,5,2)   , substr($dateToUse,8,2)+5, substr($dateToUse,0,4)));
$date6month = date("m", mktime(0, 0, 0, substr($dateToUse,5,2) , substr($dateToUse,8,2)+5, substr($dateToUse,0,4)));
$date6year = date("Y", mktime(0, 0, 0, substr($dateToUse,5,2)  , substr($dateToUse,8,2)+5, substr($dateToUse,0,4)));

$date7day = date("d", mktime(0, 0, 0, substr($dateToUse,5,2)   , substr($dateToUse,8,2)+6, substr($dateToUse,0,4)));
$date7month = date("m", mktime(0, 0, 0, substr($dateToUse,5,2) , substr($dateToUse,8,2)+6, substr($dateToUse,0,4)));
$date7year = date("Y", mktime(0, 0, 0, substr($dateToUse,5,2)  , substr($dateToUse,8,2)+6, substr($dateToUse,0,4)));
///////////////////////////Date 1 to 7 :End

  $goTo = "clientTrades";
////Request parameters, if passed : transfer to proper variable :Start
  if(isset($_GET['display']))
    $display = $_GET['display'];
  else
    $display = 'trades';
  if(isset($_GET['itemId']))
    $currentItemId = $_GET['itemId'];
  else
    $currentItemId = "All";
////Request parameters, if passed : transfer to proper variable :End
  
//////////displayBuySellValues:Start
  $displayBuySellValues[0]  = "All";
  $displayBuySellOptions[0] = "All";
  $displayBuySellValues[1]  = "Buy";
  $displayBuySellOptions[1] = "Buy";
  $displayBuySellValues[2]  = "Sell";
  $displayBuySellOptions[2] = "Sell";
  $displayBuySellSelected   = isset($_REQUEST['displayBuySell']) ? $_REQUEST['displayBuySell'] : "All";
//////////displayBuySellValues:End

  //Client records :Start
  $clientIdSelected = isset($_GET['clientId'])?$_GET['clientId']:0;
  $clientIdValues = array();
  $clientIdOptions = array();
  $i = 0;
  $clientIdValues[0]  = 0;
  $clientIdOptions[0] = 'All';
  $i++;

  $clientQuery = "SELECT * FROM client
                    ORDER BY firstName, middleName, lastName";
  $clientResult = mysql_query($clientQuery);
  while($clientRow = mysql_fetch_array($clientResult))
  {
    $clientIdValues[$i] = $clientRow['clientId'];
    $clientIdOptions[$i] = $clientRow['firstName']." ".$clientRow['middleName']." ".$clientRow['lastName'];
    $i++;
  }
  //Client records :End
  //Item records :Start
  $itemIdSelected = $currentItemId;
  $itemIdValues = array();
  $itemIdOptions = array();
  $itemCount = 0;
  $itemIdValues[0]  = "All";
  $itemIdOptions[0] = "All";
  $itemCount++;

  $itemRecords = array();
  $clientBrok  = array();
  $itemQuery = "SELECT * FROM item ORDER BY itemId";
  $itemResult = mysql_query($itemQuery);
  while($itemRow = mysql_fetch_array($itemResult))
  {
    $itemRecords[$itemRow['itemId']]['priceOn']   = $itemRow['priceOn'];
    $itemRecords[$itemRow['itemId']]['min']       = $itemRow['min'];
    $itemRecords[$itemRow['itemId']]['mulAmount'] = $itemRow['mulAmount'];
    
    //ClientBrok :Start
    for($i=0;$i<count($clientIdValues);$i++)
      $clientBrok[$clientIdValues[$i]][$itemRow['itemId']] = $itemRow['oneSideBrok'];
    //ClientBrok :End
    
    $itemIdValues[$itemCount]  = $itemRow['itemId'];
    $itemIdOptions[$itemCount] = $itemRow['itemId'];
    $itemCount++;
  }
  //Item records :End
  //Override clientBrok from clientBrok table :Start
  $clientBrokQuery = "SELECT * FROM clientbrok ORDER BY clientId";
  $clientBrokResult = mysql_query($clientBrokQuery);
  while($clientBrokRow = mysql_fetch_array($clientBrokResult))
  {
    $clientBrok[$clientBrokRow['clientId']][$clientBrokRow['itemId']] = $clientBrokRow['oneSideBrok'];
  }
  //Override clientBrok from clientBrok table :Start
  
  //Expiry records :Start
  if(isset($_GET['expiryDate']))
  {
    if($_GET['itemIdChanged']==1 || $currentItemId=="All")
      $expiryDateSelected = 0;
    else
      $expiryDateSelected = $_GET['expiryDate'];
  }
  else
    $expiryDateSelected = 0;
    
  $expiryDateValues = array();
  $expiryDateOptions = array();
  $i = 0;
  $expiryDateValues[0]  = 0;
  $expiryDateOptions[0] = 'All';
  $i++;

  if($currentItemId!="All")
  {
    $expiryQuery = "SELECT * FROM expiry
                      ORDER BY itemId, expiryDate";
    $expiryResult = mysql_query($expiryQuery);
    while($expiryRow = mysql_fetch_array($expiryResult))
    {
      if($expiryRow['itemId'] == $currentItemId)
      {
        $expiryDateValues[$i]  = $expiryRow['expiryDate'];
        $expiryDateOptions[$i] = $expiryRow['expiryDate'];
        $i++;
      }
    }
  }
  //Expiry records :End

  $trades = array();
  
  $expiryDetails = array();
  //$expiryQuery = "SELECT * FROM expiry ORDER BY itemId, str_to_date(expiryDate,'%d%b%Y')";
  

		$expiryQuery = "SELECT expiry.itemId, expiry.expiryDate,item.mulAmount FROM expiry,item where expiry.itemId=item.itemId  ORDER BY itemId, str_to_date(expiryDate,'%d%b%Y')";
	
	
    $expiryResult = mysql_query($expiryQuery);
	$inc = 0;
    while($expiryRow = mysql_fetch_array($expiryResult))
    {
        $expiryDetails[$inc]['id']  = $expiryRow['expiryId'];
		$expiryDetails[$inc]['itemId']  = $expiryRow['itemId'];
		$expiryDetails[$inc]['expiryDate']  = $expiryRow['expiryDate'];
		$expiryDetails[$inc]['mulAmount']  = $expiryRow['mulAmount'];
		$inc++;
	}
  
  $prevClientId = 0;
  $prevItemId = '';
  $prevExpiryDate = '';
  $valuesForGrossLine = array();
  $wholeItemArr       = array();
  $wholeItemArrCount  = -1;  //-1, because we do ++ when we store 0

  $wholeBuyQty        = 0;
  $wholeTotBuyAmount  = 0;
  $wholeBuyRash       = 0;
  $wholeSellQty       = 0;
  $wholeTotSellAmount = 0;
  $wholeSellRash      = 0;
  $wholeProfitLoss    = 0;
  $wholeOneSideBrok   = 0;
  $wholeNetProfitLoss = 0;
  $wholeNetLossOnly   = 0;
  $wholeNetProfitOnly = 0;

  $valuesForGrossLine['openBuyQty'] = 0;
  $valuesForGrossLine['openSellQty'] = 0;
  $valuesForGrossLine['totBuyQty'] = 0;
  $valuesForGrossLine['totSellQty'] = 0;
  $valuesForGrossLine['totBuyAmount'] = 0;
  $valuesForGrossLine['totSellAmount'] = 0;
  $i = 0;
  $whereCondition = "";
  $tradesQuery = "SELECT * FROM tradetxt
                  WHERE 1=1";
/////////////////////////////////////////////Where Condition :Start
  $whereGiven = true;
  if(isset($_GET['clientId']) && $_GET['clientId']!=0)
  {
    $whereCondition .= " AND clientId = ".$_GET['clientId'];
    $whereGiven = true;
  }
  $whereCondition .= $whereGiven ? " AND ":" WHERE ";
  $whereCondition .= "exchange = '".$currentExchange."'";
  $whereGiven = true;
  
  $whereCondition .= "  AND clientId IN (SELECT clientId FROM client WHERE clientBroker = 0) ";
  if($displayBuySellSelected != "All")
  {
    $whereCondition .= " AND buySell = '".$displayBuySellSelected."'";
  }

  if($currentItemId!="All")
  {
    if($whereGiven)
      $whereCondition .= " AND   itemId LIKE '".$currentItemId."'";
    else
      $whereCondition .= " WHERE itemId LIKE '".$currentItemId."'";
    $whereGiven = true;
  }
  if(isset($_GET['expiryDate']) && $_GET['expiryDate']!='0' && $_GET['itemIdChanged']!=1 && $currentItemId!="All")
  {
    if($whereGiven)
      $whereCondition .= " AND   expiryDate LIKE '".$_GET['expiryDate']."'";
    else
      $whereCondition .= " WHERE expiryDate LIKE '".$_GET['expiryDate']."'";
    $whereGiven = true;
  }

  if(isset($_SESSION['fromDate']))
  {//WHERE tradeDate >=  '2004-08-03' AND tradeDate <=  '2004-08-04'
    if($whereGiven)
     $whereCondition .= " AND tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'" ;
    else
    {
      $whereCondition .= " WHERE tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'" ;
      $whereGiven = true;
    }
  }

  if($display == 'openStand')
  {
    if($whereGiven)
     $whereCondition .= " AND standing = -1";
    else
    {
      $whereCondition .= " WHERE standing = -1";
      $whereGiven = true;
    }
  }
  if($display == 'closeStand')
  {
    if($whereGiven)
     $whereCondition .= " AND standing = 1";
    else
    {
      $whereCondition .= " WHERE standing = 1";
      $whereGiven = true;
    }
  }
  if($display == 'openCloseStand')
  {
    if($whereGiven)
     $whereCondition .= " AND (standing = -1 OR standing = 1)";
    else
    {
      $whereCondition .= " WHERE (standing = -1 OR standing = 1)";
      $whereGiven = true;
    }
  }
  
  if($display == 'noInnerStand')
  {
    if($whereGiven)
      $whereCondition .= " AND NOT ((standing = -1 OR standing = 1) 
                       AND tradeDate != '".$_SESSION['fromDate']."'
                       AND tradeDate != '".$_SESSION['toDate']."')";
    else
    {
      $whereCondition .= " WHERE NOT ((standing = -1 OR standing = 1) 
                       AND tradeDate != '".$_SESSION['fromDate']."'
                       AND tradeDate != '".$_SESSION['toDate']."')";
      $whereGiven = true;
    }
  }
  
  $tradesQuery .= $whereCondition;
/////////////////////////////////////////////Where Condition :End
  applyConfirm();
  
  $tradesQuery .= " ORDER BY firstName, middleName, lastName, itemId ASC, expiryDate, 
                     tradeDate ASC, standing, tradeTime, tradeId";
  $tradesResult = mysql_query($tradesQuery);
  if(mysql_num_rows($tradesResult) == 0)
    $message = "No records!";
  else
  {
    while($tradesRow = mysql_fetch_array($tradesResult))
    {
      $trades[$i]['tradeId']         = $tradesRow['tradeId'];
      $trades[$i]['dispGross']       = 0;
      $trades[$i]['dispClientWhole'] = 0;
      $trades[$i]['confirmed']       = $tradesRow['confirmed'];
      
      if($i==0)
      { $valuesForGrossLine['clientPrevProfitLoss'] = 0;  $valuesForGrossLine['clientPrevBrok'] = 0;  }
  
      //For Gross line :Start //Gross section comes initially in while loop, but it is used after displaying trades ... it is here because when client or item or expiry change, first we store data to display gross for previous matter
      if($tradesRow['clientId'] != $prevClientId || $tradesRow['itemId'] != $prevItemId || $tradesRow['expiryDate'] != $prevExpiryDate)
      {
        if($prevClientId != 0)
        {
          $trades = updateForGrossLine($trades, $i, $valuesForGrossLine);
          $valuesForGrossLine['clientPrevProfitLoss'] = $trades[$i-1]['clientTotProfitLoss'];
          $valuesForGrossLine['clientPrevBrok']       = $trades[$i-1]['clientTotBrok'];
          
          $valuesForGrossLine['openBuyQty'] = 0;
          $valuesForGrossLine['openSellQty'] = 0;
          $valuesForGrossLine['totBuyQty']  = 0;
          $valuesForGrossLine['totSellQty'] = 0;
          $valuesForGrossLine['totBuyAmount'] = 0;
          $valuesForGrossLine['totSellAmount'] = 0;
          
          if($tradesRow['clientId'] != $prevClientId)
          {
            $trades[$i-1]['dispClientWhole'] = 1;
            $valuesForGrossLine['clientPrevProfitLoss'] = 0;  $valuesForGrossLine['clientPrevBrok'] = 0;
          }
        }
      }
      //For Gross line :End
      //priceOn and min are here because we need to assign value after gross line, and also, at first time : we take first priceOn and min and then we use it in gross line
      $priceOn = $itemRecords[$tradesRow['itemId']]['priceOn'];
      $valuesForGrossLine['min'] = $itemRecords[$tradesRow['itemId']]['min'];
  
      //We take first oneSideBrok and then use in Gross line
      $valuesForGrossLine['oneSideBrok'] = $clientBrok[$tradesRow['clientId']][$tradesRow['itemId']];
      
      $trades[$i]['clientId']     = $tradesRow['clientId'];
      $trades[$i]['prevClientId'] = $prevClientId;
      $prevClientId               = $tradesRow['clientId'];
      $trades[$i]['itemId']       = $tradesRow['itemId'];
      $trades[$i]['prevItemId']   = $prevItemId;
      $prevItemId                 = $tradesRow['itemId'];
      $trades[$i]['expiryDate']   = $tradesRow['expiryDate'];
      $trades[$i]['prevExpiryDate']   = $prevExpiryDate;
      $prevExpiryDate             = $tradesRow['expiryDate'];
      
      $trades[$i]['mulAmount']    = $itemRecords[$tradesRow['itemId']]['mulAmount'];
  
      $trades[$i]['clientName'] = $tradesRow['firstName']." ".$tradesRow['middleName']." ".$tradesRow['lastName'];
      $trades[$i]['vendor']    = $tradesRow['vendor'];
      $trades[$i]['tradeDate'] = mysqlToDDMMYY($tradesRow['tradeDate']);
      $trades[$i]['tradeTime'] = $tradesRow['tradeTime'];
      $trades[$i]['standing']  = standToDisplay($tradesRow['standing']);
      $trades[$i]['itemIdExpiry'] = $tradesRow['itemId']."-".substr($tradesRow['expiryDate'],2,3);
      $trades[$i]['buySell']   = $tradesRow['buySell'];
  
      $trades[$i]['tradeRefNo']  = $tradesRow['tradeRefNo'];
      $trades[$i]['userRemarks'] = $tradesRow['userRemarks'];
      $trades[$i]['ownClient']   = $tradesRow['ownClient'];
  
      if($tradesRow['buySell'] == 'Buy')
      {
        $trades[$i]['fontColor'] = "blue";
        $trades[$i]['buyQty']    = $tradesRow['qty'];
        $trades[$i]['price']     = $tradesRow['price'];
        $trades[$i]['sellQty']   = '&nbsp;';
        $trades[$i]['sellPrice'] = '&nbsp;';
        
        if($tradesRow['standing'] == -1)
          $valuesForGrossLine['openBuyQty'] += $tradesRow['qty'];
          
        $valuesForGrossLine['totBuyQty'] += $tradesRow['qty'];
        $valuesForGrossLine['totBuyAmount'] += $tradesRow['price']*$tradesRow['qty'];
      }
      else
      {
        $trades[$i]['fontColor'] = "red";
        $trades[$i]['buyQty'] = '&nbsp;';
        $trades[$i]['price'] = '&nbsp;';
        $trades[$i]['sellQty'] = $tradesRow['qty'];
        $trades[$i]['sellPrice'] = $tradesRow['price'];
        
        if($tradesRow['standing'] == -1)
          $valuesForGrossLine['openSellQty'] += $tradesRow['qty'];
          
        $valuesForGrossLine['totSellQty'] += $tradesRow['qty'];
        $valuesForGrossLine['totSellAmount'] += $tradesRow['price']*$tradesRow['qty'];
      }
      $i++;
    }
    $totTrades = $i;
    $trades = updateForGrossLine($trades, $i, $valuesForGrossLine);
    $trades[$i-1]['dispClientWhole'] = 1;

    //number_format :Start
    for($i=0;$i<$totTrades;$i++)
    {
      $trades[$i]['totBuyAmount']  = number_format(isset($trades[$i]['totBuyAmount'])?$trades[$i]['totBuyAmount']:0,4,'.','');
      $trades[$i]['totSellAmount'] = number_format(isset($trades[$i]['totSellAmount'])?$trades[$i]['totSellAmount']:0,4,'.','');
      $trades[$i]['buyRash']       = number_format(isset($trades[$i]['buyRash'])?$trades[$i]['buyRash']:0,4,'.','');
      $trades[$i]['sellRash']      = number_format(isset($trades[$i]['sellRash'])?$trades[$i]['sellRash']:0,4,'.','');
      $trades[$i]['profitLoss']    = number_format(isset($trades[$i]['profitLoss'])?$trades[$i]['profitLoss']:0,4,'.','');
      $trades[$i]['oneSideBrok']   = number_format(isset($trades[$i]['oneSideBrok'])?$trades[$i]['oneSideBrok']:0,4,'.','');
      $trades[$i]['netProfitLossNotFormatted'] = isset($trades[$i]['netProfitLoss'])?$trades[$i]['netProfitLoss']:0;
      $trades[$i]['netProfitLoss'] = number_format(isset($trades[$i]['netProfitLoss'])?$trades[$i]['netProfitLoss']:0,4,'.','');
      if(isset($trades[$i]['netProfitLoss']) && $trades[$i]['netProfitLoss'] >= 0)
        $trades[$i]['netProfitLossInRs'] = number_format($trades[$i]['netProfitLoss']*$_SESSION['profitBankRate'],4,'.','');
      elseif(isset($trades[$i]['netProfitLoss']) && $trades[$i]['netProfitLoss'] < 0)
        $trades[$i]['netProfitLossInRs'] = number_format($trades[$i]['netProfitLoss']*$_SESSION['lossBankRate'],4,'.','');
      else
        $trades[$i]['netProfitLossInRs'] = 0;
        
      $trades[$i]['clientTotProfitLoss'] = number_format(isset($trades[$i]['clientTotProfitLoss'])?$trades[$i]['clientTotProfitLoss']:0,4,'.','');
      $trades[$i]['clientTotBrok']       = number_format(isset($trades[$i]['clientTotBrok'])?$trades[$i]['clientTotBrok']:0,4,'.','');

//////////////////////////
      if(isset($trades[$i]['clientTotNetProfitLoss']) && $trades[$i]['clientTotNetProfitLoss'] >= 0)
        $trades[$i]['clientTotNetProfitLossInRs'] = number_format($trades[$i]['clientTotNetProfitLoss']*$_SESSION['profitBankRate'],4,'.','');
      elseif(isset($trades[$i]['clientTotNetProfitLoss']) && $trades[$i]['clientTotNetProfitLoss'] < 0)
        $trades[$i]['clientTotNetProfitLossInRs'] = number_format($trades[$i]['clientTotNetProfitLoss']*$_SESSION['lossBankRate'],4,'.','');
      else
        $trades[$i]['clientTotNetProfitLossInRs'] = 0;
//////////////////////////
      $trades[$i]['clientTotNetProfitLoss'] = number_format(isset($trades[$i]['clientTotNetProfitLoss'])?$trades[$i]['clientTotNetProfitLoss']:0,4,'.','');
    }
    for($i=0;$i<=$wholeItemArrCount;$i++)
    {
  //////////////For whole total :Start
      $wholeBuyQty        += $wholeItemArr[$i]['buyQty'];
      $wholeTotBuyAmount  += $wholeItemArr[$i]['totBuyAmount'];
      $wholeSellQty       += $wholeItemArr[$i]['sellQty'];
      $wholeTotSellAmount += $wholeItemArr[$i]['totSellAmount'];
      $wholeProfitLoss    += $wholeItemArr[$i]['profitLoss'];
      $wholeOneSideBrok   += $wholeItemArr[$i]['oneSideBrok'];
      $wholeNetProfitLoss += $wholeItemArr[$i]['netProfitLoss'];
      if($wholeItemArr[$i]['netProfitLoss'] >= 0) $wholeNetProfitOnly += $wholeItemArr[$i]['netProfitLoss'];
      else $wholeNetLossOnly += $wholeItemArr[$i]['netProfitLoss'];

  //////////////For whole total :End
  //////////////For number_format :Start
      $wholeItemArr[$i]['totBuyAmount']  = number_format($wholeItemArr[$i]['totBuyAmount'],4,'.','');
      $wholeItemArr[$i]['totSellAmount'] = number_format($wholeItemArr[$i]['totSellAmount'],4,'.','');
      $wholeItemArr[$i]['buyRash']       = number_format($wholeItemArr[$i]['buyRash'],4,'.','');
      $wholeItemArr[$i]['sellRash']      = number_format($wholeItemArr[$i]['sellRash'],4,'.','');
      $wholeItemArr[$i]['profitLoss']    = number_format($wholeItemArr[$i]['profitLoss'],4,'.','');
      $wholeItemArr[$i]['oneSideBrok']   = number_format($wholeItemArr[$i]['oneSideBrok'],4,'.','');
      $wholeItemArr[$i]['netProfitLoss'] = number_format($wholeItemArr[$i]['netProfitLoss'],4,'.','');
  //////////////For number_format :End
    }
    $wholeBuyRash  = ($wholeBuyQty!=0)?($wholeTotBuyAmount/$wholeBuyQty):0;
    $wholeSellRash = ($wholeSellQty!=0)?($wholeTotSellAmount/$wholeSellQty):0;
  }
  //number_format :End
  $smarty->assign("expiryDetails",$expiryDetails);
  $smarty->assign("PHP_SELF", $_SERVER['PHP_SELF']);
  $smarty->assign("display", $display);
  $smarty->assign("message", $message);
  $smarty->assign("currentExchange", $currentExchange);
  $smarty->assign("clientIdSelected", $clientIdSelected);
  $smarty->assign("clientIdValues",   $clientIdValues);
  $smarty->assign("clientIdOptions",  $clientIdOptions);
  $smarty->assign("itemIdSelected",   $itemIdSelected);
  $smarty->assign("itemIdValues",     $itemIdValues);
  $smarty->assign("itemIdOptions",    $itemIdOptions);
  $smarty->assign("expiryDateSelected", $expiryDateSelected);
  $smarty->assign("expiryDateValues",   $expiryDateValues);
  $smarty->assign("expiryDateOptions",  $expiryDateOptions);
  $smarty->assign("goTo",  $goTo);
  
  $smarty->assign("fromDate", substr($_SESSION['fromDate'],8,2)."-".substr($_SESSION['fromDate'],5,2)."-".substr($_SESSION['fromDate'],2,2));
  $smarty->assign("toDate",   substr($_SESSION['toDate'],8,2)."-".substr($_SESSION['toDate'],5,2)."-".substr($_SESSION['toDate'],2,2));
  $smarty->assign("profitLossDate", substr($_SESSION['toDate'],8,2)."-".substr($_SESSION['toDate'],5,2)."-".substr($_SESSION['toDate'],0,4));
  
  $smarty->assign("trades",           $trades);
  $smarty->assign("wholeItemArr",     $wholeItemArr);

  $smarty->assign("wholeBuyQty",        $wholeBuyQty);
  $smarty->assign("wholeTotBuyAmount",  number_format($wholeTotBuyAmount,4,'.',''));
  $smarty->assign("wholeBuyRash",       number_format($wholeBuyRash,4,'.',''));
  $smarty->assign("wholeSellQty",       $wholeSellQty);
  $smarty->assign("wholeTotSellAmount", number_format($wholeTotSellAmount,4,'.',''));
  $smarty->assign("wholeSellRash",      number_format($wholeSellRash,4,'.',''));
  $smarty->assign("wholeProfitLoss",    number_format($wholeProfitLoss,4,'.',''));
  $smarty->assign("wholeOneSideBrok",   number_format($wholeOneSideBrok,4,'.',''));
  $smarty->assign("wholeNetProfitLoss", number_format($wholeNetProfitLoss,4,'.',''));
  $smarty->assign("wholeNetProfitOnly", number_format($wholeNetProfitOnly,4,'.',''));
  $smarty->assign("wholeNetLossOnly",   number_format($wholeNetLossOnly,4,'.',''));
  
  $smarty->assign("displayBuySellValues",   $displayBuySellValues);
  $smarty->assign("displayBuySellOptions",  $displayBuySellOptions);
  $smarty->assign("displayBuySellSelected", $displayBuySellSelected);
  $smarty->assign("date1day", $date1day);
  $smarty->assign("date1month", $date1month);
  $smarty->assign("date1year", $date1year);
  $smarty->assign("date2day", $date2day);
  $smarty->assign("date2month", $date2month);
  $smarty->assign("date2year", $date2year);
  $smarty->assign("date3day", $date3day);
  $smarty->assign("date3month", $date3month);
  $smarty->assign("date3year", $date3year);
  $smarty->assign("date4day", $date4day);
  $smarty->assign("date4month", $date4month);
  $smarty->assign("date4year", $date4year);
  $smarty->assign("date5day", $date5day);
  $smarty->assign("date5month", $date5month);
  $smarty->assign("date5year", $date5year);
  $smarty->assign("date6day", $date6day);
  $smarty->assign("date6month", $date6month);
  $smarty->assign("date6year", $date6year);
  $smarty->assign("date7day", $date7day);
  $smarty->assign("date7month", $date7month);
  $smarty->assign("date7year", $date7year);

/////////////////Use tpl as per 'display' parameter :Start
  if($display == 'trades')
    $smarty->display("clientTrades-dynamic.tpl");
  elseif($display == 'detailed')
    $smarty->display("clientTrades.tpl");//We display detailed view from same file for minimum view
  elseif($display == 'gross')
    $smarty->display("clientGross.tpl");
  elseif($display == 'itemWiseGross')
    $smarty->display("clientItemWiseGross.tpl");
  elseif($display == 'itemPending')
    $smarty->display("clientItemPending.tpl");
  elseif($display == 'openStand' || $display == 'closeStand' || $display == 'openCloseStand')
    $smarty->display("clientStand.tpl");
  elseif($display == 'PLToAccount')
    $smarty->display("clientPLToAccount.tpl");
  elseif($display == 'tradesPrint')
    $smarty->display("clientTradesPrint.tpl");
  elseif($display == 'detailed')
    $smarty->display("clientTradesPrint.tpl");//We display detailed view from same file for minimum view
  elseif($display == 'grossPrint')
    $smarty->display("clientGrossPrint.tpl");
  elseif($display == 'itemWiseGross')
    $smarty->display("clientItemWiseGrossPrint.tpl");
  elseif($display == 'openStand' || $display == 'closeStand' || $display == 'openCloseStand')
    $smarty->display("clientStandPrint.tpl");
  else
    $smarty->display("clientTrades.tpl");
/////////////////Use tpl as per 'display' parameter :End
}
///////////////////////////////
function updateForGrossLine($trades, $i, $valuesForGrossLine)
{
  global $wholeItemArr, $wholeItemArrCount;
  $trades[$i-1]['dispGross']     = 1;
  $trades[$i-1]['totBuyQty']     = $valuesForGrossLine['totBuyQty'];
  $trades[$i-1]['totSellQty']    = $valuesForGrossLine['totSellQty'];
  $trades[$i-1]['totBuyAmount']  = $valuesForGrossLine['totBuyAmount'];
  $trades[$i-1]['totSellAmount'] = $valuesForGrossLine['totSellAmount'];
  $trades[$i-1]['buyRash']       = ($valuesForGrossLine['totBuyQty']!=0)?($valuesForGrossLine['totBuyAmount']/$valuesForGrossLine['totBuyQty']):0;
  $trades[$i-1]['sellRash']      = ($valuesForGrossLine['totSellQty']!=0)?($valuesForGrossLine['totSellAmount']/$valuesForGrossLine['totSellQty']):0;
  
  if($trades[$i-1]['totBuyQty'] == $trades[$i-1]['totSellQty'])
  {
    $trades[$i-1]['profitLoss'] = $valuesForGrossLine['totSellAmount'] - $valuesForGrossLine['totBuyAmount'];
    $trades[$i-1]['profitLoss'] = $trades[$i-1]['profitLoss']*$trades[$i-1]['mulAmount'];
    
    //We subtract openBuyQty and openSellQty... but actually there will be only 1 open qty : buy or sell ...so result will be ok
    $trades[$i-1]['oneSideBrok'] 
     = $valuesForGrossLine['oneSideBrok']
       * ($valuesForGrossLine['totBuyQty'] - $valuesForGrossLine['openBuyQty']
          - $valuesForGrossLine['openSellQty'])
       / $valuesForGrossLine['min'];
    
    $trades[$i-1]['netProfitLoss']= $trades[$i-1]['profitLoss'] - $trades[$i-1]['oneSideBrok'];
  }
  else
  {
    $trades[$i-1]['profitLoss']   = 0;
    $trades[$i-1]['oneSideBrok']  = 0;
    $trades[$i-1]['netProfitLoss']= 0;
  }
  
  $trades[$i-1]['clientTotProfitLoss'] = $valuesForGrossLine['clientPrevProfitLoss'] + $trades[$i-1]['profitLoss'];

  $trades[$i-1]['clientTotBrok']       = $valuesForGrossLine['clientPrevBrok'] + $trades[$i-1]['oneSideBrok'];
  $trades[$i-1]['clientTotNetProfitLoss'] = $trades[$i-1]['clientTotProfitLoss'] - $trades[$i-1]['clientTotBrok'];

  if(!array_search_recursive($trades[$i-1]['itemIdExpiry'], $wholeItemArr))
  {
    $wholeItemArrCount++;
    $wholeItemArr[$wholeItemArrCount]['itemIdExpiry']  = $trades[$i-1]['itemIdExpiry'];
    $wholeItemArr[$wholeItemArrCount]['buyQty']        = 0;
    $wholeItemArr[$wholeItemArrCount]['sellQty']       = 0;
    $wholeItemArr[$wholeItemArrCount]['totBuyAmount']  = 0;
    $wholeItemArr[$wholeItemArrCount]['totSellAmount'] = 0;
    $wholeItemArr[$wholeItemArrCount]['profitLoss']    = 0;
    $wholeItemArr[$wholeItemArrCount]['oneSideBrok']   = 0;
    $wholeItemArr[$wholeItemArrCount]['netProfitLoss'] = 0;
    
    $elementNoToUse = $wholeItemArrCount;
  }
  else
  {
    $foundArray = array_search_recursive($trades[$i-1]['itemIdExpiry'], $wholeItemArr);
    $elementNoToUse = $foundArray[0];
  }
    
  $wholeItemArr[$elementNoToUse]['buyQty']        += $trades[$i-1]['totBuyQty'];
  $wholeItemArr[$elementNoToUse]['sellQty']       += $trades[$i-1]['totSellQty'];
  $wholeItemArr[$elementNoToUse]['totBuyAmount']  += $trades[$i-1]['totBuyAmount'];
  $wholeItemArr[$elementNoToUse]['totSellAmount'] += $trades[$i-1]['totSellAmount'];
  $wholeItemArr[$elementNoToUse]['buyRash']       = ($wholeItemArr[$wholeItemArrCount]['buyQty']!=0)?($wholeItemArr[$wholeItemArrCount]['totBuyAmount']/$wholeItemArr[$wholeItemArrCount]['buyQty']):0;
  $wholeItemArr[$elementNoToUse]['sellRash']      = ($wholeItemArr[$wholeItemArrCount]['sellQty']!=0)?($wholeItemArr[$wholeItemArrCount]['totSellAmount']/$wholeItemArr[$wholeItemArrCount]['sellQty']):0;
  $wholeItemArr[$elementNoToUse]['profitLoss']    += $trades[$i-1]['profitLoss'];
  $wholeItemArr[$elementNoToUse]['oneSideBrok']   += $trades[$i-1]['oneSideBrok'];
  $wholeItemArr[$elementNoToUse]['netProfitLoss'] += $trades[$i-1]['netProfitLoss'];

  return $trades;
}

///////////////////////////
function applyConfirm()
{
	global $whereCondition;
  if(isset($_REQUEST['confirmBtn1']) || isset($_REQUEST['confirmBtn2']))
  {
  	$updateQuery  = "UPDATE tradetxt SET confirmed = 0 
  	                  WHERE 1 = 1 ";
  	$updateQuery .= $whereCondition;
  	mysql_query($updateQuery);
  	
  	if(isset($_REQUEST['confirmed']))
  	{
  		$confirmedArray = implode(",", $_REQUEST['confirmed']);
	  	$updateQuery  = "UPDATE tradetxt SET confirmed = 1
	  	                  WHERE tradeId IN (".$confirmedArray.")";
  	
    	mysql_query($updateQuery);
    }
    
    $headerText  = isset($_REQUEST['display'])        ? "display=".$_REQUEST['display'] : "";
    $headerText .= isset($_REQUEST['itemIdChanged'])  ? "&itemIdChanged=".$_REQUEST['itemIdChanged'] : "";
    $headerText .= isset($_REQUEST['clientId'])       ? "&clientId=".$_REQUEST['clientId'] : "";
    $headerText .= isset($_REQUEST['itemId'])         ? "&itemId=".$_REQUEST['itemId'] : "";
    $headerText .= isset($_REQUEST['expiryDate'])     ? "&expiryDate=".$_REQUEST['expiryDate'] : "";
    header("Location: ./clientTrades.php?".$headerText);
    exit();
  }
}
?>