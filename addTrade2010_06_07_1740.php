<?php
include "etc/om_config.inc";
//if(!isset($_SESSION['user']))
//  header("Location: login.php");
//else
//{
  $smarty = new SmartyWWW();
  $item   = array();
  $trade  = array();
  date_default_timezone_set('Asia/Kolkata');
  if(isset($_GET['forStand']) && $_GET['forStand'] == 1)
    $forStand = 1;
  else
    $forStand = 0;
  $tradeId = 0;
  $isEdit  = 0;
  $refTradeId = 0;
  $lot     = 1;
  $selectedItem = "";
  $buyOrSell = "Buy";
  $goTo = "";
  if(isset($_REQUEST['tradeId']))
  {
    $tradeId = $_REQUEST['tradeId'];
    $goTo    = $_REQUEST['goTo'];
    $isEdit  = 1;
    $selectTradeQuery = "SELECT tradeId,standing,clientId,firstName,middleName,lastName,
                                clientId2,firstName2,middleName2,lastName2,buySell,tradetxt.itemId,
                                tradeTime,qty,price,price2,tradeRefNo,tradeDate,item.itemId,
                                expiryDate,userRemarks,confirmed,tradetxt.exchange,refTradeId,selfRefId,item.min
                           FROM tradetxt
                           JOIN item ON item.itemId = tradetxt.itemId
                          WHERE tradeId = ".$tradeId;
    $selectTradeQueryResult = mysql_query($selectTradeQuery);
    if($tradeRow = mysql_fetch_assoc($selectTradeQueryResult))
      $trade = $tradeRow;
    $defaultClientId2 = $trade['clientId2'];
    $buyOrSell = $trade['buySell'];
    $refTradeId   = $trade['refTradeId'];
    $selectedItem = $trade['itemId'];
    $lot          = $trade['qty']/$trade['min'];
  }
  if(isset($_GET['exchange']))
    $exchange = $_GET['exchange'];
  $selectItemQuery = "SELECT itemId,item,itemShort,brok,brok2,oneSideBrok,min,priceOn,mulAmount,
                             rangeStart,rangeEnd,qtyInLots,exchangeId,exchange
                        FROM item
                       WHERE exchange = '".$exchange."'
                       ORDER BY itemId";
  $selectItemQueryResult = mysql_query($selectItemQuery);
  $i = 0;
  while($itemRow = mysql_fetch_object($selectItemQueryResult))
  {
    $itemIdForCombo[$i]      = $itemRow->itemId;
    $item['itemId'][$i]      = $itemRow->itemId;
    $item['item'][$i]        = $itemRow->item;
    $item['itemShort'][$i]   = $itemRow->itemShort;
    $item['brok'][$i]        = $itemRow->brok;
    $item['brok2'][$i]       = $itemRow->brok2;
    $item['oneSideBrok'][$i] = $itemRow->oneSideBrok;
    $item['min'][$i]         = $itemRow->min;
    $item['priceOn'][$i]     = $itemRow->priceOn;
    $item['mulAmount'][$i]   = $itemRow->mulAmount;
    $item['rangeStart'][$i]  = $itemRow->rangeStart != "" ? $itemRow->rangeStart : 0;
    $item['rangeEnd'][$i]    = $itemRow->rangeEnd != "" ? $itemRow->rangeEnd : 0;
    $item['qtyInLots'][$i]   = $itemRow->qtyInLots;
    $item['exchangeId'][$i]  = $itemRow->exchangeId;
    $item['exchange'][$i]    = $itemRow->exchange;
    if($exchange == "MCX")
    {
      $selectExchangeQuery = "SELECT itemId,expiryDate,exchange
                                FROM expiry
                               WHERE itemId ='".$itemRow->itemId."'";
      $selectExchangeQueryResult = mysql_query($selectExchangeQuery);
      $j = 0;
      while($expiryRow = mysql_fetch_object($selectExchangeQueryResult))
      {
        $expiryDate[$i][$j] = $expiryRow->expiryDate;
        $j++;
      }
    }
    else if($exchange == "CX")
    {
      $selectExchangeQuery = "SELECT itemId,expiryDate,exchange
                                FROM expiry
                               WHERE itemId ='".$itemRow->itemId."'
                                 AND exchange = 'CX'";
      $selectExchangeQueryResult = mysql_query($selectExchangeQuery);
      $j = 0;
      while($expiryRow = mysql_fetch_object($selectExchangeQueryResult))
      {
        $expiryDate[$i][$j] = $expiryRow->expiryDate;
        $j++;
      }
    }
    else if($exchange == "F_O")
    {
      $selectExchangeQuery = "SELECT itemId,expiryDate,exchange
                                FROM expiry
                               WHERE itemId = 'F_O'";
      $selectExchangeQueryResult = mysql_query($selectExchangeQuery);
      $j = 0;
      while($expiryRow = mysql_fetch_object($selectExchangeQueryResult))
      {
        $expiryDate[$j] = $expiryRow->expiryDate;
        $j++;
      }
    }
    $i++;
  }
  $selectClientQuery = "SELECT clientId,firstName,middleName,lastName
                          FROM client
                         ORDER BY firstName,middleName,lastName";
  $selectClientQueryResult = mysql_query($selectClientQuery);
  $clientIdValues = array();
  $clientIdOutput = array();
  $i = 0;
  while($row = mysql_fetch_object($selectClientQueryResult))
  {
    $clientIdValues[$i] = $row->clientId;
    $clientIdOutput[$i] = $row->firstName." ".$row->middleName." ".$row->lastName;
    $i++;
  }
  $clientId2Values = array_reverse($clientIdValues);
  $clientId2Output = array_reverse($clientIdOutput);
  $smarty->assign("PHP_SELF",$_SERVER['PHP_SELF']);
  $smarty->assign("exchange",$exchange);
  $smarty->assign("forStand",$forStand);
  $smarty->assign("askTime",$askTime);
  $smarty->assign("item",$item);
  $smarty->assign("buyOrSell",$buyOrSell);
  $smarty->assign("itemIdForCombo",$itemIdForCombo);
  $smarty->assign("expiryDate",$expiryDate);
  $smarty->assign("clientIdValues",$clientIdValues);
  $smarty->assign("clientIdOutput",$clientIdOutput);
  $smarty->assign("clientId2Values",$clientId2Values);
  $smarty->assign("clientId2Output",$clientId2Output);
  $smarty->assign("defaultClientId2",$defaultClientId2);
  $smarty->assign("twoClientTextBox",$twoClientTextBox);
  $smarty->assign("clientIdAskInTextBox",$clientIdAskInTextBox);
  $smarty->assign("trade",$trade);
  $smarty->assign("isEdit",$isEdit);
  $smarty->assign("selectedItem",$selectedItem);
  $smarty->assign("lot",$lot);
  $smarty->assign("tradeId",$tradeId);
  $smarty->assign("refTradeId",$refTradeId);
  $smarty->assign("cfgStoreOnEnter",$cfgStoreOnEnter);
  $smarty->assign("goTo",$goTo);
  $smarty->display("addTrade.tpl");
//}
?>