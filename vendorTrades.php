<?php
include "./etc/om_config.inc";
if(!isset($_SESSION['toDate'])) 
{
  header("Location: selectDtSession.php?goTo=vendorTrades");
}
else
{
  include "./etc/functions.inc";
  $smarty = new SmartyWWW();

  include "./tradeToVendor.php";
  
  $message = "";
  
////Request parameters, if passed : transfer to proper variable :Start
  if(isset($_GET['display']))
    $display = $_GET['display'];
  else
    $display = 'trades';
  if(isset($_GET['itemId']))
    $currentItemId = $_GET['itemId'];
  else
    $currentItemId = "All";
////Request parameters, if passed : transfer to proper variable :End
  
  //Vendor records :Start
  $vendorSelected = isset($_GET['vendor'])?$_GET['vendor']:0;
  $vendorValues = array();
  $vendorOptions = array();
  $i = 0;
  $vendorValues[0]  = 'All';
  $vendorOptions[0] = 'All';
  $i++;

  $vendorQuery = "SELECT * FROM vendor
                    ORDER BY vendor";
  $vendorResult = mysql_query($vendorQuery);
  while($vendorRow = mysql_fetch_array($vendorResult))
  {
    $vendorValues[$i] = $vendorRow['vendor'];
    $vendorOptions[$i] = $vendorRow['vendor'];
    $i++;
  }
  //Vendor records :End
  //Item records :Start
  $itemIdSelected = $currentItemId;
  $itemIdValues = array();
  $itemIdOptions = array();
  $itemCount = 0;
  $itemIdValues[0]  = "All";
  $itemIdOptions[0] = "All";
  $itemCount++;

  $itemRecords = array();
  $vendorBrok  = array();
  $itemQuery = "SELECT * FROM item ORDER BY itemId";
  $itemResult = mysql_query($itemQuery);
  while($itemRow = mysql_fetch_array($itemResult))
  {
    $itemRecords[$itemRow['itemId']]['priceOn'] = $itemRow['priceOn'];
    $itemRecords[$itemRow['itemId']]['min']     = $itemRow['min'];
    $itemRecords[$itemRow['itemId']]['mulAmount'] = $itemRow['mulAmount'];
    
    //VendorBrok :Start
    for($i=0;$i<count($vendorValues);$i++)
      $vendorBrok[$vendorValues[$i]][$itemRow['itemId']] = $itemRow['oneSideBrok'];
    //VendorBrok :End
    
    $itemIdValues[$itemCount]  = $itemRow['itemId'];
    $itemIdOptions[$itemCount] = $itemRow['itemId'];
    $itemCount++;
  }
  //Item records :End
  //Override vendorBrok from vendorBrok table :Start
  $vendorBrokQuery = "SELECT * FROM vendorbrok ORDER BY vendor";
  $vendorBrokResult = mysql_query($vendorBrokQuery);
  while($vendorBrokRow = mysql_fetch_array($vendorBrokResult))
  {
    $vendorBrok[$vendorBrokRow['vendor']][$vendorBrokRow['itemId']] = $vendorBrokRow['oneSideBrok'];
  }
  //Override vendorBrok from vendorBrok table :Start
  
  //Expiry records :Start
  if(isset($_GET['expiryDate']))
  {
    if($_GET['itemIdChanged']==1 || $currentItemId=="All")
      $expiryDateSelected = 0;
    else
      $expiryDateSelected = $_GET['expiryDate'];
  }
  else
    $expiryDateSelected = 0;
    
  $expiryDateValues = array();
  $expiryDateOptions = array();
  $i = 0;
  $expiryDateValues[0]  = 0;
  $expiryDateOptions[0] = 'All';
  $i++;

  if($currentItemId!="All")
  {
    $expiryQuery = "SELECT * FROM expiry
                      ORDER BY itemId, expiryDate";
    $expiryResult = mysql_query($expiryQuery);
    while($expiryRow = mysql_fetch_array($expiryResult))
    {
      if($expiryRow['itemId'] == $currentItemId)
      {
        $expiryDateValues[$i]  = $expiryRow['expiryDate'];
        $expiryDateOptions[$i] = $expiryRow['expiryDate'];
        $i++;
      }
    }
  }
  //Expiry records :End

  $trades = array();
  $prevVendor = '';
  $prevItemId = '';
  $prevExpiryDate = '';
  $valuesForGrossLine = array();
  $wholeItemArr       = array();
  $wholeItemArrCount  = -1;  //-1, because we do ++ when we store 0

  $wholeBuyQty        = 0;
  $wholeTotBuyAmount  = 0;
  $wholeBuyRash       = 0;
  $wholeSellQty       = 0;
  $wholeTotSellAmount = 0;
  $wholeSellRash      = 0;
  $wholeProfitLoss    = 0;
  $wholeOneSideBrok   = 0;
  $wholeNetProfitLoss = 0;
  $wholeNetLossOnly   = 0;
  $wholeNetProfitOnly = 0;

  $valuesForGrossLine['openBuyQty'] = 0;
  $valuesForGrossLine['openSellQty'] = 0;
  $valuesForGrossLine['totBuyQty'] = 0;
  $valuesForGrossLine['totSellQty'] = 0;
  $valuesForGrossLine['totBuyAmount'] = 0;
  $valuesForGrossLine['totSellAmount'] = 0;
  $i = 0;
  $tradesQuery = "SELECT * FROM vendortemp";
/////////////////////////////////////////////Where Condition :Start
  $whereGiven = false;
  if(isset($_GET['vendor']) && $_GET['vendor']!='All')
  {
    $tradesQuery .= " WHERE vendor LIKE '".$_GET['vendor']."'";
    $whereGiven = true;
  }
  if($currentItemId!="All")
  {
    if($whereGiven)
      $tradesQuery .= " AND   itemId LIKE '".$currentItemId."'";
    else
      $tradesQuery .= " WHERE itemId LIKE '".$currentItemId."'";
    $whereGiven = true;
  }
  if(isset($_GET['expiryDate']) && $_GET['expiryDate']!='0' && $_GET['itemIdChanged']!=1 && $currentItemId!="All")
  {
    if($whereGiven)
      $tradesQuery .= " AND   expiryDate LIKE '".$_GET['expiryDate']."'";
    else
      $tradesQuery .= " WHERE expiryDate LIKE '".$_GET['expiryDate']."'";
    $whereGiven = true;
  }

  if(isset($_SESSION['fromDate']))
  {//WHERE tradeDate >=  '2004-08-03' AND tradeDate <=  '2004-08-04'
    if($whereGiven)
     $tradesQuery .= " AND tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'" ;
    else
    {
      $tradesQuery .= " WHERE tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'" ;
      $whereGiven = true;
    }
  }

  if($display == 'openStand')
  {
    if($whereGiven)
     $tradesQuery .= " AND standing = -1";
    else
    {
      $tradesQuery .= " WHERE standing = -1";
      $whereGiven = true;
    }
  }
  if($display == 'closeStand')
  {
    if($whereGiven)
     $tradesQuery .= " AND standing = 1";
    else
    {
      $tradesQuery .= " WHERE standing = 1";
      $whereGiven = true;
    }
  }
  if($display == 'openCloseStand')
  {
    if($whereGiven)
     $tradesQuery .= " AND (standing = -1 OR standing = 1)";
    else
    {
      $tradesQuery .= " WHERE (standing = -1 OR standing = 1)";
      $whereGiven = true;
    }
  }
  
  if($display == 'noInnerStand')
  {
    if($whereGiven)
      $tradesQuery .= " AND NOT ((standing = -1 OR standing = 1) 
                       AND tradeDate != '".$_SESSION['fromDate']."'
                       AND tradeDate != '".$_SESSION['toDate']."')";
    else
    {
      $tradesQuery .= " WHERE NOT ((standing = -1 OR standing = 1) 
                       AND tradeDate != '".$_SESSION['fromDate']."'
                       AND tradeDate != '".$_SESSION['toDate']."')";
      $whereGiven = true;
    }
  }
/////////////////////////////////////////////Where Condition :End
  $tradesQuery .= " ORDER BY vendor, itemId ASC, expiryDate, 
                     tradeDate ASC, standing, tradeTime, tradeId";
  $tradesResult = mysql_query($tradesQuery);

  if(mysql_num_rows($tradesResult) == 0)
    $message = "No records!";
  else
  {
    while($tradesRow = mysql_fetch_array($tradesResult))
    {
      $trades[$i]['tradeId']         = $tradesRow['tradeId'];
      $trades[$i]['dispGross']       = 0;
      $trades[$i]['dispVendorWhole'] = 0;
      
      if($i==0)
      { $valuesForGrossLine['vendorPrevProfitLoss'] = 0;  $valuesForGrossLine['vendorPrevBrok'] = 0;  }
  
      //For Gross line :Start //Gross section comes initially in while loop, but it is used after displaying trades ... it is here because when vendor or item or expiry change, first we store data to display gross for previous matter
      if($tradesRow['vendor'] != $prevVendor || $tradesRow['itemId'] != $prevItemId || $tradesRow['expiryDate'] != $prevExpiryDate)
      {
        if($prevVendor != '')
        {
          $trades = updateForGrossLine($trades, $i, $valuesForGrossLine);
          $valuesForGrossLine['vendorPrevProfitLoss'] = $trades[$i-1]['vendorTotProfitLoss'];
          $valuesForGrossLine['vendorPrevBrok']       = $trades[$i-1]['vendorTotBrok'];
          
          $valuesForGrossLine['openBuyQty'] = 0;
          $valuesForGrossLine['openSellQty'] = 0;
          $valuesForGrossLine['totBuyQty']  = 0;
          $valuesForGrossLine['totSellQty'] = 0;
          $valuesForGrossLine['totBuyAmount'] = 0;
          $valuesForGrossLine['totSellAmount'] = 0;
          
          if($tradesRow['vendor'] != $prevVendor)
          {
            $trades[$i-1]['dispVendorWhole'] = 1;
            $valuesForGrossLine['vendorPrevProfitLoss'] = 0;  $valuesForGrossLine['vendorPrevBrok'] = 0;
          }
        }
      }
      //For Gross line :End
      //priceOn and min are here because we need to assign value after gross line, and also, at first time : we take first priceOn and min and then we use it in gross line
      $priceOn = $itemRecords[$tradesRow['itemId']]['priceOn'];
      $valuesForGrossLine['min'] = $itemRecords[$tradesRow['itemId']]['min'];
  
      //We take first oneSideBrok and then use in Gross line
      $valuesForGrossLine['oneSideBrok'] = $vendorBrok[$tradesRow['vendor']][$tradesRow['itemId']];
      
      $trades[$i]['vendor']     = $tradesRow['vendor'];
      $trades[$i]['prevVendor'] = $prevVendor;
      $prevVendor               = $tradesRow['vendor'];
      $trades[$i]['itemId']       = $tradesRow['itemId'];
      $trades[$i]['prevItemId']   = $prevItemId;
      $prevItemId                 = $tradesRow['itemId'];
      $trades[$i]['expiryDate']   = $tradesRow['expiryDate'];
      $trades[$i]['prevExpiryDate']   = $prevExpiryDate;
      $prevExpiryDate             = $tradesRow['expiryDate'];
  
      $trades[$i]['mulAmount']    = $itemRecords[$tradesRow['itemId']]['mulAmount'];

      $trades[$i]['vendorName'] = $tradesRow['vendor'];
      $trades[$i]['tradeDate'] = mysqlToDDMMYY($tradesRow['tradeDate']);
      $trades[$i]['tradeTime'] = $tradesRow['tradeTime'];
      $trades[$i]['standing']  = standToDisplay($tradesRow['standing']);
      $trades[$i]['itemIdExpiry'] = $tradesRow['itemId']."-".substr($tradesRow['expiryDate'],2,3);
      $trades[$i]['buySell']   = $tradesRow['buySell'];
  
      $trades[$i]['clientId']   = $tradesRow['clientId'];
      $trades[$i]['clientName'] = $tradesRow['firstName']." ".$tradesRow['middleName']." ".$tradesRow['lastName'];

      $trades[$i]['tradeRefNo']  = $tradesRow['tradeRefNo'];
      $trades[$i]['userRemarks'] = $tradesRow['userRemarks'];
      $trades[$i]['ownClient']   = $tradesRow['ownClient'];
  
      if($tradesRow['buySell'] == 'Buy')
      {
        $trades[$i]['fontColor'] = "blue";
        $trades[$i]['buyQty']    = $tradesRow['qty'];
        $trades[$i]['price']     = $tradesRow['price'];
        $trades[$i]['sellQty']   = '&nbsp;';
        $trades[$i]['sellPrice'] = '&nbsp;';
        
        if($tradesRow['standing'] == -1)
          $valuesForGrossLine['openBuyQty'] += $tradesRow['qty'];
          
        $valuesForGrossLine['totBuyQty'] += $tradesRow['qty'];
        $valuesForGrossLine['totBuyAmount'] += $tradesRow['price']*$tradesRow['qty'];
      }
      else
      {
        $trades[$i]['fontColor'] = "red";
        $trades[$i]['buyQty'] = '&nbsp;';
        $trades[$i]['price'] = '&nbsp;';
        $trades[$i]['sellQty'] = $tradesRow['qty'];
        $trades[$i]['sellPrice'] = $tradesRow['price'];
        
        if($tradesRow['standing'] == -1)
          $valuesForGrossLine['openSellQty'] += $tradesRow['qty'];
          
        $valuesForGrossLine['totSellQty'] += $tradesRow['qty'];
        $valuesForGrossLine['totSellAmount'] += $tradesRow['price']*$tradesRow['qty'];
      }
      $i++;
    }
    $totTrades = $i;
    $trades = updateForGrossLine($trades, $i, $valuesForGrossLine);
    $trades[$i-1]['dispVendorWhole'] = 1;

    //number_format :Start
    for($i=0;$i<$totTrades;$i++)
    {
      $trades[$i]['totBuyAmount']  = number_format(isset($trades[$i]['totBuyAmount'])?$trades[$i]['totBuyAmount']:0,4,'.','');
      $trades[$i]['totSellAmount'] = number_format(isset($trades[$i]['totSellAmount'])?$trades[$i]['totSellAmount']:0,4,'.','');
      $trades[$i]['buyRash']       = number_format(isset($trades[$i]['buyRash'])?$trades[$i]['buyRash']:0,4,'.','');
      $trades[$i]['sellRash']      = number_format(isset($trades[$i]['sellRash'])?$trades[$i]['sellRash']:0,4,'.','');
      $trades[$i]['profitLoss']    = number_format(isset($trades[$i]['profitLoss'])?$trades[$i]['profitLoss']:0,4,'.','');
      $trades[$i]['oneSideBrok']   = number_format(isset($trades[$i]['oneSideBrok'])?$trades[$i]['oneSideBrok']:0,4,'.','');
      $trades[$i]['netProfitLoss'] = number_format(isset($trades[$i]['netProfitLoss'])?$trades[$i]['netProfitLoss']:0,4,'.','');
      if(isset($trades[$i]['netProfitLoss']) && $trades[$i]['netProfitLoss'] >= 0)
        $trades[$i]['netProfitLossInRs'] = number_format($trades[$i]['netProfitLoss']*$_SESSION['profitBankRate'],4,'.','');
      elseif(isset($trades[$i]['netProfitLoss']) && $trades[$i]['netProfitLoss'] < 0)
        $trades[$i]['netProfitLossInRs'] = number_format($trades[$i]['netProfitLoss']*$_SESSION['lossBankRate'],4,'.','');
      else
        $trades[$i]['netProfitLossInRs'] = 0;
        
      $trades[$i]['vendorTotProfitLoss'] = number_format(isset($trades[$i]['vendorTotProfitLoss'])?$trades[$i]['vendorTotProfitLoss']:0,4,'.','');
      $trades[$i]['vendorTotBrok']       = number_format(isset($trades[$i]['vendorTotBrok'])?$trades[$i]['vendorTotBrok']:0,4,'.','');

//////////////////////////
      if(isset($trades[$i]['vendorTotNetProfitLoss']) && $trades[$i]['vendorTotNetProfitLoss'] >= 0)
        $trades[$i]['vendorTotNetProfitLossInRs'] = number_format($trades[$i]['vendorTotNetProfitLoss']*$_SESSION['profitBankRate'],4,'.','');
      elseif(isset($trades[$i]['vendorTotNetProfitLoss']) && $trades[$i]['vendorTotNetProfitLoss'] < 0)
        $trades[$i]['vendorTotNetProfitLossInRs'] = number_format($trades[$i]['vendorTotNetProfitLoss']*$_SESSION['lossBankRate'],4,'.','');
      else
        $trades[$i]['vendorTotNetProfitLossInRs'] = 0;
//////////////////////////
      $trades[$i]['vendorTotNetProfitLoss'] = number_format(isset($trades[$i]['vendorTotNetProfitLoss'])?$trades[$i]['vendorTotNetProfitLoss']:0,4,'.','');
    }
    for($i=0;$i<=$wholeItemArrCount;$i++)
    {
  //////////////For whole total :Start
      $wholeBuyQty        += $wholeItemArr[$i]['buyQty'];
      $wholeTotBuyAmount  += $wholeItemArr[$i]['totBuyAmount'];
      $wholeSellQty       += $wholeItemArr[$i]['sellQty'];
      $wholeTotSellAmount += $wholeItemArr[$i]['totSellAmount'];
      $wholeProfitLoss    += $wholeItemArr[$i]['profitLoss'];
      $wholeOneSideBrok   += $wholeItemArr[$i]['oneSideBrok'];
      $wholeNetProfitLoss += $wholeItemArr[$i]['netProfitLoss'];
      if($wholeItemArr[$i]['netProfitLoss'] >= 0) $wholeNetProfitOnly += $wholeItemArr[$i]['netProfitLoss'];
      else $wholeNetLossOnly += $wholeItemArr[$i]['netProfitLoss'];

  //////////////For whole total :End
  //////////////For number_format :Start
      $wholeItemArr[$i]['totBuyAmount']  = number_format($wholeItemArr[$i]['totBuyAmount'],4,'.','');
      $wholeItemArr[$i]['totSellAmount'] = number_format($wholeItemArr[$i]['totSellAmount'],4,'.','');
      $wholeItemArr[$i]['buyRash']       = number_format($wholeItemArr[$i]['buyRash'],4,'.','');
      $wholeItemArr[$i]['sellRash']      = number_format($wholeItemArr[$i]['sellRash'],4,'.','');
      $wholeItemArr[$i]['profitLoss']    = number_format($wholeItemArr[$i]['profitLoss'],4,'.','');
      $wholeItemArr[$i]['oneSideBrok']   = number_format($wholeItemArr[$i]['oneSideBrok'],4,'.','');
      $wholeItemArr[$i]['netProfitLoss'] = number_format($wholeItemArr[$i]['netProfitLoss'],4,'.','');
  //////////////For number_format :End
    }
    $wholeBuyRash  = ($wholeBuyQty!=0)?($wholeTotBuyAmount/$wholeBuyQty):0;
    $wholeSellRash = ($wholeSellQty!=0)?($wholeTotSellAmount/$wholeSellQty):0;
  }
  //number_format :End
  
  $smarty->assign("PHP_SELF", $_SERVER['PHP_SELF']);
  $smarty->assign("display", $display);
  $smarty->assign("message", $message);
  $smarty->assign("vendorSelected", $vendorSelected);
  $smarty->assign("vendorValues",   $vendorValues);
  $smarty->assign("vendorOptions",  $vendorOptions);
  $smarty->assign("itemIdSelected",   $itemIdSelected);
  $smarty->assign("itemIdValues",     $itemIdValues);
  $smarty->assign("itemIdOptions",    $itemIdOptions);
  $smarty->assign("expiryDateSelected", $expiryDateSelected);
  $smarty->assign("expiryDateValues",   $expiryDateValues);
  $smarty->assign("expiryDateOptions",  $expiryDateOptions);
  
  $smarty->assign("fromDate", substr($_SESSION['fromDate'],8,2)."-".substr($_SESSION['fromDate'],5,2)."-".substr($_SESSION['fromDate'],2,2));
  $smarty->assign("toDate",   substr($_SESSION['toDate'],8,2)."-".substr($_SESSION['toDate'],5,2)."-".substr($_SESSION['toDate'],2,2));
  
  $smarty->assign("trades",           $trades);
  $smarty->assign("wholeItemArr",     $wholeItemArr);

  $smarty->assign("wholeBuyQty",        $wholeBuyQty);
  $smarty->assign("wholeTotBuyAmount",  number_format($wholeTotBuyAmount,4,'.',''));
  $smarty->assign("wholeBuyRash",       number_format($wholeBuyRash,4,'.',''));
  $smarty->assign("wholeSellQty",       $wholeSellQty);
  $smarty->assign("wholeTotSellAmount", number_format($wholeTotSellAmount,4,'.',''));
  $smarty->assign("wholeSellRash",      number_format($wholeSellRash,4,'.',''));
  $smarty->assign("wholeProfitLoss",    number_format($wholeProfitLoss,4,'.',''));
  $smarty->assign("wholeOneSideBrok",   number_format($wholeOneSideBrok,4,'.',''));
  $smarty->assign("wholeNetProfitLoss", number_format($wholeNetProfitLoss,4,'.',''));
  $smarty->assign("wholeNetProfitOnly", number_format($wholeNetProfitOnly,4,'.',''));
  $smarty->assign("wholeNetLossOnly",   number_format($wholeNetLossOnly,4,'.',''));

/////////////////Use tpl as per 'display' parameter :Start
  if($display == 'trades')
    $smarty->display("vendorTrades.tpl");
  elseif($display == 'detailed')
    $smarty->display("vendorTrades.tpl");//We display detailed view from same file for minimum view
  elseif($display == 'gross')
    $smarty->display("vendorGross.tpl");
  elseif($display == 'itemWiseGross')
    $smarty->display("vendorItemWiseGross.tpl");
  elseif($display == 'itemPending')
    $smarty->display("vendorItemPending.tpl");
  elseif($display == 'openStand' || $display == 'closeStand' || $display == 'openCloseStand')
    $smarty->display("vendorStand.tpl");
  elseif($display == 'tradesPrint')
    $smarty->display("vendorTradesPrint.tpl");
  elseif($display == 'detailed')
    $smarty->display("vendorTradesPrint.tpl");//We display detailed view from same file for minimum view
  elseif($display == 'grossPrint')
    $smarty->display("vendorGrossPrint.tpl");
  elseif($display == 'itemWiseGross')
    $smarty->display("vendorItemWiseGrossPrint.tpl");
  elseif($display == 'openStand' || $display == 'closeStand' || $display == 'openCloseStand')
    $smarty->display("vendorStandPrint.tpl");
  else
    $smarty->display("vendorTrades.tpl");
/////////////////Use tpl as per 'display' parameter :End
}
///////////////////////////////
function updateForGrossLine($trades, $i, $valuesForGrossLine)
{
  global $wholeItemArr, $wholeItemArrCount;
  $trades[$i-1]['dispGross']     = 1;
  $trades[$i-1]['totBuyQty']     = $valuesForGrossLine['totBuyQty'];
  $trades[$i-1]['totSellQty']    = $valuesForGrossLine['totSellQty'];
  $trades[$i-1]['totBuyAmount']  = $valuesForGrossLine['totBuyAmount'];
  $trades[$i-1]['totSellAmount'] = $valuesForGrossLine['totSellAmount'];
  $trades[$i-1]['buyRash']       = ($valuesForGrossLine['totBuyQty']!=0)?($valuesForGrossLine['totBuyAmount']/$valuesForGrossLine['totBuyQty']):0;
  $trades[$i-1]['sellRash']      = ($valuesForGrossLine['totSellQty']!=0)?($valuesForGrossLine['totSellAmount']/$valuesForGrossLine['totSellQty']):0;
  
  if($trades[$i-1]['totBuyQty'] == $trades[$i-1]['totSellQty'])
  {
    $trades[$i-1]['profitLoss']   = $valuesForGrossLine['totSellAmount'] - $valuesForGrossLine['totBuyAmount'];
    $trades[$i-1]['profitLoss'] = $trades[$i-1]['profitLoss']*$trades[$i-1]['mulAmount'];
    
    //We subtract openBuyQty and openSellQty... but actually there will be only 1 open qty : buy or sell ...so result will be ok
    $trades[$i-1]['oneSideBrok'] 
     = $valuesForGrossLine['oneSideBrok']
       * ($valuesForGrossLine['totBuyQty'] - $valuesForGrossLine['openBuyQty']
          - $valuesForGrossLine['openSellQty'])
       / $valuesForGrossLine['min'];
    
    $trades[$i-1]['netProfitLoss']= $trades[$i-1]['profitLoss'] + $trades[$i-1]['oneSideBrok'];
  }
  else
  {
    $trades[$i-1]['profitLoss']   = 0;
    $trades[$i-1]['oneSideBrok']  = 0;
    $trades[$i-1]['netProfitLoss']= 0;
  }
  
  $trades[$i-1]['vendorTotProfitLoss'] = $valuesForGrossLine['vendorPrevProfitLoss'] + $trades[$i-1]['profitLoss'];
  $trades[$i-1]['vendorTotBrok']       = $valuesForGrossLine['vendorPrevBrok'] + $trades[$i-1]['oneSideBrok'];
  $trades[$i-1]['vendorTotNetProfitLoss'] = $trades[$i-1]['vendorTotProfitLoss'] + $trades[$i-1]['vendorTotBrok'];

  if(!array_search_recursive($trades[$i-1]['itemIdExpiry'], $wholeItemArr))
  {
    $wholeItemArrCount++;
    $wholeItemArr[$wholeItemArrCount]['itemIdExpiry']  = $trades[$i-1]['itemIdExpiry'];
    $wholeItemArr[$wholeItemArrCount]['buyQty']        = 0;
    $wholeItemArr[$wholeItemArrCount]['sellQty']       = 0;
    $wholeItemArr[$wholeItemArrCount]['totBuyAmount']  = 0;
    $wholeItemArr[$wholeItemArrCount]['totSellAmount'] = 0;
    $wholeItemArr[$wholeItemArrCount]['profitLoss']    = 0;
    $wholeItemArr[$wholeItemArrCount]['oneSideBrok']   = 0;
    $wholeItemArr[$wholeItemArrCount]['netProfitLoss'] = 0;
  }
  $wholeItemArr[$wholeItemArrCount]['buyQty']        += $trades[$i-1]['totBuyQty'];
  $wholeItemArr[$wholeItemArrCount]['sellQty']       += $trades[$i-1]['totSellQty'];
  $wholeItemArr[$wholeItemArrCount]['totBuyAmount']  += $trades[$i-1]['totBuyAmount'];
  $wholeItemArr[$wholeItemArrCount]['totSellAmount'] += $trades[$i-1]['totSellAmount'];
  $wholeItemArr[$wholeItemArrCount]['buyRash']       = ($wholeItemArr[$wholeItemArrCount]['buyQty']!=0)?($wholeItemArr[$wholeItemArrCount]['totBuyAmount']/$wholeItemArr[$wholeItemArrCount]['buyQty']):0;
  $wholeItemArr[$wholeItemArrCount]['sellRash']      = ($wholeItemArr[$wholeItemArrCount]['sellQty']!=0)?($wholeItemArr[$wholeItemArrCount]['totSellAmount']/$wholeItemArr[$wholeItemArrCount]['sellQty']):0;
  $wholeItemArr[$wholeItemArrCount]['profitLoss']    += $trades[$i-1]['profitLoss'];
  $wholeItemArr[$wholeItemArrCount]['oneSideBrok']   += $trades[$i-1]['oneSideBrok'];
  $wholeItemArr[$wholeItemArrCount]['netProfitLoss'] += $trades[$i-1]['netProfitLoss'];

  return $trades;
}
?>