<HTML>
<HEAD><TITLE>Om : Vendor Trades !!!</TITLE>
<STYLE>
{literal}
td{font-weight: NORMAL}
.lossStyle   {color: black}
.profitStyle {color: black}
{/literal}
</STYLE>  
</HEAD>
<BODY>
<FORM name="form1" method="get" action="{$PHP_SELF}">
<INPUT type="hidden" name="display" value="{$display}">
<INPUT type="hidden" name="itemIdChanged" value="0">
<TABLE width="100%" cellPadding="0" cellSpacing="0" border="0">
<TR>
  <TD>Vendor : 
    <SELECT name="vendor" onChange="document.form1.submit();">
    {html_options selected="$vendorSelected" values="$vendorValues" output="$vendorOptions"}
    </SELECT>
  </TD>
  <TD>Item : 
    <SELECT name="itemId" onChange="document.form1.itemIdChanged.value=1;document.form1.submit();">
    {html_options selected="$itemIdSelected" values="$itemIdValues" output="$itemIdOptions"}
    </SELECT>
  </TD>
  <TD>Expiry : 
    <SELECT name="expiryDate" onChange="document.form1.submit();">
    {html_options selected="$expiryDateSelected" values="$expiryDateValues" output="$expiryDateOptions"}
    </SELECT>
  </TD>
</TR>
<TR>
  <TD colspan="3" align="center">
    <A href="selectDtSession.php">Date range</A> : {$fromDate} To : {$toDate}</CENTER>
  </TD>
</TR>
<TR>
  <TD colspan="3" align="center">
    {$message}
  </TD>
</TR>
<TABLE border="0" cellPadding="2" cellSpacing="0">
<TR>
  <TD>&nbsp;</TD>
  <TD colspan="2" align="center">Buy</TD>
  <TD colspan="2" align="center">Sell</TD>
  <TD colspan="6">&nbsp;</TD>
</TR>
<TR>
  <TD align="center">BuySell</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD align="center">Date</TD>
  <TD align="center">Stand</TD>
  <TD align="center">Item</TD>
  <TD align="center">NetProfitLoss</TD>
  <TD align="center">&nbsp;</TD>
  <TD align="center">&nbsp;</TD>
  {if $display == "detailed"} 
    <TD align="center">UserRemarks</TD>
    <TD align="center">OwnClient</TD>
    <TD align="center" NOWRAP>TradeRefNo</TD>
  {/if}
</TR>
{section name="sec1" loop="$trades"}
{if $trades[sec1].vendor != $trades[sec1].prevVendor or $trades[sec1].itemId != $trades[sec1].prevItemId or $trades[sec1].expiryDate != $trades[sec1].prevExpiryDate}
  <TR>
    <TD colspan="5"><U>{$trades[sec1].vendor}</U></TD>
    <TD colspan="6" align="center">{$trades[sec1].itemIdExpiry}</TD>
  </TR>
{/if}
<TR style="color:{$trades[sec1].fontColor}">
  <TD align="center">{$trades[sec1].buySell}</TD>
  <TD align="right">{$trades[sec1].buyQty}</TD>
  <TD align="right">{$trades[sec1].price}</TD>
  <TD align="right">{$trades[sec1].sellQty}</TD>
  <TD align="right">{$trades[sec1].sellPrice}</TD>
  <TD NOWRAP>{$trades[sec1].tradeDate}
    {if $display == "detailed"} {$trades[sec1].tradeTime} {/if}
  </TD>
  <TD>{$trades[sec1].standing}</TD>
  <TD align="center" NOWRAP>{$trades[sec1].itemIdExpiry}</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  {if $display == "detailed"} 
    <TD align="center">{$trades[sec1].userRemarks}</TD>
    <TD align="center">{$trades[sec1].ownClient}</TD>
    <TD align="center" NOWRAP>{$trades[sec1].tradeRefNo}</TD>
  {/if}
</TR>
{if $trades[sec1].dispGross != 0}
  <TR>
    <TD align="right" NOWRAP>
      Net : {math equation="totBuyQty-totSellQty" totBuyQty=$trades[sec1].totBuyQty totSellQty=$trades[sec1].totSellQty}
    </TD>
    <TD align="right">{$trades[sec1].totBuyQty}</TD>
    <TD align="right">{$trades[sec1].buyRash}</TD>
    <TD align="right">{$trades[sec1].totSellQty}</TD>
    <TD align="right">{$trades[sec1].sellRash}</TD>
  {if $trades[sec1].totBuyQty == $trades[sec1].totSellQty} 
    <TD colspan="3" align="right" NOWRAP>
      {if $trades[sec1].profitLoss < 0}
        <FONT class="lossStyle">Loss : 
      {else}
        <FONT class="profitStyle">Profit : 
      {/if}
      {$trades[sec1].profitLoss}</FONT>
      Brok       : {$trades[sec1].oneSideBrok}</TD>
    <TD align="right" NOWRAP>
      {if $trades[sec1].netProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
      {$trades[sec1].netProfitLoss}</FONT>
    </TD>
    <TD colspan="2" align="right">InRs : {$trades[sec1].netProfitLossInRs}</TD>
  {else}
    <TD colspan="6">{$trades[sec1].itemIdExpiry} : Buy Sell Qty Not Same</TD>
  {/if}
  </TR>
  {if $trades[sec1].dispVendorWhole != 0}
  <TR>
    <TD colspan="5" align="right">
      {$trades[sec1].vendor}
      : Total : 
    </TD>
    <TD colspan="3" align="right"><U>
      {if $trades[sec1].vendorTotProfitLoss < 0}
        <FONT class="lossStyle">Loss : 
      {else}
        <FONT class="profitStyle">Profit : 
      {/if}
      {$trades[sec1].vendorTotProfitLoss}</FONT></U>
      Brok       : {$trades[sec1].vendorTotBrok}</U>
    </TD>
    <TD align="right"><U>
      {if $trades[sec1].vendorTotNetProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
      {$trades[sec1].vendorTotNetProfitLoss}</FONT></U></TD>
    <TD colspan="2" align="right">InRs : {$trades[sec1].vendorTotNetProfitLossInRs}</TD>
  </TR>
  {/if}
{/if}
{/section}
<TR><TD colspan="11">&nbsp;</TD></TR>
<TR>
  <TD align="center">Net</TD>
  <TD>Buy</TD>
  <TD>Rash</TD>
  <TD>Sell</TD>
  <TD>Rash</TD>
  <TD colspan="2" align="center">Item</TD>
  <TD align="center">ProfitLoss</TD>
  <TD colspan="2" align="center">Brok</TD>
  <TD align="center">NetProfitLoss</TD>
</TR>
{section name="sec2" loop="$wholeItemArr"}
<TR>
    <TD align="right" NOWRAP>
      {math equation="buyQty-sellQty" buyQty=$wholeItemArr[sec2].buyQty sellQty=$wholeItemArr[sec2].sellQty}
    </TD>
  <TD align="right">{$wholeItemArr[sec2].buyQty}</TD>
  <TD align="right">{$wholeItemArr[sec2].buyRash}</TD>
  <TD align="right">{$wholeItemArr[sec2].sellQty}</TD>
  <TD align="right">{$wholeItemArr[sec2].sellRash}</TD>
  <TD align="right" colspan="2" NOWRAP>{$wholeItemArr[sec2].itemIdExpiry}</TD>
  <TD align="right" NOWRAP>
    {if $wholeItemArr[sec2].profitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
    {$wholeItemArr[sec2].profitLoss}</FONT></TD>
  <TD align="right" colspan="2">{$wholeItemArr[sec2].oneSideBrok}</TD>
  <TD align="right" NOWRAP>
    {if $wholeItemArr[sec2].netProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
    {$wholeItemArr[sec2].netProfitLoss}</FONT></TD>
</TR>
{/section}
<TR>
    <TD align="right" NOWRAP>
      {math equation="buyQty-sellQty" buyQty=$wholeBuyQty sellQty=$wholeSellQty}
    </TD>
  <TD align="right">{$wholeBuyQty}</TD>
  <TD align="right">{$wholeBuyRash}</TD>
  <TD align="right">{$wholeSellQty}</TD>
  <TD align="right">{$wholeSellRash}</TD>
  <TD align="right" colspan="2" NOWRAP> : Total : </TD>
  <TD align="right" NOWRAP>
    {if $wholeProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
    {$wholeProfitLoss}</FONT></TD>
  <TD align="right" colspan="2">{$wholeOneSideBrok}</TD>
  <TD align="right" NOWRAP>
    {if $wholeNetProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
    {$wholeNetProfitLoss}</FONT></TD>
</TR>
</TABLE>
</FORM>
</BODY>
</HTML>
