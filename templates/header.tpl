<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<title>Om !!!</title>
  <style type="text/css" >
  ul.topnav
  {
    list-style: none;
    padding: 0 20px;
    margin: 0;
    float: left;
    width: 920px;
    background: #222;
    font-size: 1.2em;
    background: url(images/topnav_bg.gif) repeat-x;
  }
  ul.topnav li
  {
    float: left;
    margin: 0;
    padding: 0 15px 0 0;
    position: relative; /*--Declare X and Y axis base for sub navigation--*/
  }
  ul.topnav li a{
    padding: 10px 5px;
    color: #fff;
    display: block;
    text-decoration: none;
    float: left;
  }
  ul.topnav li a:hover{
    background: url(images/topnav_hover.gif) no-repeat center top;
  }
  ul.topnav li span
  { /*--Drop down trigger styles--*/
    width: 17px;
    height: 35px;
    float: left;
    background: url(images/subnav_btn.gif) no-repeat center top;
  }
  ul.topnav li span.subhover
  {background-position: center bottom; cursor: pointer;} /*--Hover effect for trigger--*/
  ul.topnav li ul.subnav
  {
    list-style: none;
    position: absolute; /*--Important - Keeps subnav from affecting main navigation flow--*/
    left: 0; top: 35px;
    background: #333;
    margin: 0; padding: 0;
    display: none;
    float: left;
    width: 190px;
    border: 1px solid #111;
  }
  ul.topnav li ul.subnav li{
    margin: 0; padding: 0;
    border-top: 1px solid #252525; /*--Create bevel effect--*/
    border-bottom: 1px solid #444; /*--Create bevel effect--*/
    clear: both;
    width: 190px;
  }
  html ul.topnav li ul.subnav li a
  {
    float: left;
    width: 165px;
    background: #333 url(images/dropdown_linkbg.gif) no-repeat 10px center;
    padding-left: 20px;
  }
  html ul.topnav li ul.subnav li a:hover
  { /*--Hover effect for subnav links--*/
    background: #222 url(images/dropdown_linkbg.gif) no-repeat 10px center;
  }

  </style>
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript">
  $(document).ready(function()
  {
    $("ul.subnav").parent().append("<span><\/span>"); //Only shows drop down trigger when js is enabled (Adds empty span tag after ul.subnav*)

    $("ul.topnav li span").click(function()
    { //When trigger is clicked...
  
      //Following events are applied to the subnav itself (moving subnav up and down)
      $(this).parent().find("ul.subnav").slideDown('fast').show(); //Drop down the subnav on click
  
      $(this).parent().hover(function()
      {
      },function()
      {
        $(this).parent().find("ul.subnav").slideUp('slow'); //When the mouse hovers out of the subnav, move it back up
      });

    //Following events are applied to the trigger (Hover events for the trigger)
    }).hover(function()
    {
      $(this).addClass("subhover"); //On hover over, add class "subhover"
    }, function()
    { //On Hover Out
      $(this).removeClass("subhover"); //On hover out, remove class "subhover"
    });

  });
  </script>
</HEAD>
<body> <!--  bgColor="#FFFF80" -->

<ul class="topnav">
  <li><a href="index.php">Home</a></li>
  <li>
    <a href="javascript:void(0);">Masters</a>
    <ul class="subnav">
      <li><a href='clientList.php'>Client List</a></li>
      <li><a href='brokerage.php'>Item List</a></li>
      <li><a href='expiryList.php'>Expiry Dates</a></li>
      <li><a href="vendorList.php">Vendor List</a></li>
      <li><a href="bankMasterList.php">Bank Master</a></li>
      <li><a href="setBankRate.php">Bank Rate</a></li>
      <li><a href="links.php">Useful Links</a></li>
    </ul>
  </li>
  <li>
    <a href="javascript:void(0);">Trades</a>
    <ul class="subnav">
      <li><a href='tradeAdd.php'>Add Trade</a></li>
      <li><a onclick='window.open("./addTrade.php?exchange=CX","","width=800,height=120,top=300"); return false;' href='#'>New Add Trade</a></li>
      <li><a href='clientTrades.php'>Client Trades</a></li>
      <li><a href='clientTrades-dynamic.php'>Client Trades Dynamic</a></li>
      <li><a href='brokerTrades.php'>Broker Trades</a></li>
      <li><a href='clientTrades.php?display=itemPending'>Item Pending</a></li>
      <li><a href="vendorTrades.php">Vendor Trades</a></li>
      <li><a href='clientTrades.php?display=gross'>Gross</a></li>
    </ul>
  </li>
  <li>
    <a href="javascript:void(0);">Other Options</a>
    <ul class="subnav">
      <li><a href='attachNameToTxtDataAll.php'>Change Name</a></li>
      <li><a href='tradeListForCrossChecking.php'>Confirm</a></li>
      <li><a href='deleteAll.php'>Delete Trades</a></li>
    </ul>
  </li>
  <li>
    <a href="mnuStand.php">Standing</a>
    <ul class="subnav">
      <li><a href='mnuStand.php'>Standing</a></li>
      <li><a href='clientTrades.php?display=tradesPrint'>Comex Trades Print</a></li>
      <li><a href='clientTrades.php?display=openStand'>Open Stand</a></li>
      <li><a href='clientTrades.php?display=closeStand'>Close Stand</a></li>
      <li><a href='clientTrades.php?display=openColoseStand'>Open &amp; Close</a></li>
      <li><a href='clientTrades.php?display=itemWiseGross'>Item Gross</a></li>
      <li><a href='clientTrades.php?display=noInnerStand'>No Inner Stand</a></li>
      <li><a href='vendorTrades.php?display=noInnerStand'>Vendor No Inner Stand</a></li>
    </ul>
  </li>
  <li>
    <a href="javascript:void(0);">Utilities</a>
    <ul class="subnav">
      <li><a href="http://localhost/pma/db_details_export.php?lang=en-utf-8&amp;server=1&amp;collation_connection=utf8_general_ci&amp;db=omcomexvip&amp;goto=db_details_export.php&amp;selectall=1">Backup</a></li>
      <li><a href="http://localhost/pma/db_details.php?lang=en-utf-8&amp;server=1&amp;collation_connection=utf8_general_ci&amp;db=omcomexvip&amp;goto=db_details_export.php&amp;db_query_force=1">Restore</a></li>
      <li><a href="extraMenu.php">Extra Menu</a></li>
      <li><a href="vendorTrades.php?display=tradesPrint">Print</a></li>
      <li><a href='goldSilverCalcJs.php'>Calculator1</a></li>
      <li><a href='calc.php'>Calculator2</a></li>
    </ul>
  </li>
  <li>
    <a href='mnuAccount.php'>Money Transactions</a>
  </li>
  <li><a href="selectDtSession.php">Date Range</a></li>
</ul>
<div style="clear:both" ></div>
<br />
<div align="center"><img align="center" src="./images/Om.jpg" /></div>
