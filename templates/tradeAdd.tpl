<HTML>
<HEAD><TITLE>Om !!! Add Trade</TITLE>
<SCRIPT language="javascript">
{literal}
window.name = 'displayAll';

function changeName()
{
  document.form1.changedField.value = "clientId";
  document.form1.submit();
}
function changeItem()
{
  document.form1.changedField.value = "itemId";
  document.form1.submit();
}

function changePrice()
{
  var price;
  price = parseFloat(document.form1.price.value);
  if(parseFloat(price) || price==0)   //parseFloat does not consider 0, so we have put 0 in or condition
  {
    if(event.keyCode==38)
      price=price+1;
    if(event.keyCode==33)
      price=price+10;
    if(event.keyCode==40)
      price=price-1;
    if(event.keyCode==34)
      price=price-10;
    if(document.form1.price.value != price)
      document.form1.price.value = price;
  }
}
function itemFromPrice()
{
  var price;
  price = parseFloat(document.form1.price.value);
{/literal}
{$itemFromPriceJS}
{literal}
}

function changeQty()
{
  {/literal}
  var minQty = {$minQty};
  {literal}
  var qty;
  qty = parseFloat(document.form1.qty.value);
  if(parseFloat(qty) || qty==0)   //parseFloat does not consider 0, so we have put 0 in or condition
  {
    if(event.keyCode==38)
      qty=qty+minQty;
    if(event.keyCode==33)
      qty=qty+minQty*5;
    if(event.keyCode==40)
      qty=qty-minQty;
    if(event.keyCode==34)
      qty=qty-minQty*5;
    if(document.form1.qty.value != qty)
      document.form1.qty.value = qty;
  }
}
{/literal}
</SCRIPT>
</HEAD>
<BODY bgColor="#FFFF80">
	<a href="./index.php">Home</a> &nbsp;<a href="./clientTrades.php">Trades</a>
  <FORM name="form1" action="{$PHP_SELF}" METHOD="post">
  <INPUT type="hidden" name="changedField" value="">
  <INPUT type="hidden" name="makeTrade" value="0">
  <INPUT type="hidden" name="firstName" value="{$firstName}">
  <INPUT type="hidden" name="middleName" value="{$middleName}">
  <INPUT type="hidden" name="lastName" value="{$lastName}">
  <INPUT type="hidden" name="forStand" value="{$forStand}">
  <TABLE BORDER=1 width="100%" cellPadding="2" cellSpacing="0">
  <TR>
    <TD>
      <SELECT name="clientId" onChange="changeName();">
      {html_options selected="$clientIdSelected" values="$clientIdValues" output="$clientIdOutput" }
      </SELECT>
    </TD>
    <TD>Date : 
      {html_select_date time="$tradeDateDisplay" prefix="trade" start_year="-1" end_year="+1" month_format="%m" field_order="DMY" day_value_format="%02d"}
    </TD>
    <TD colspan="2" NOWRAP>
      Price : <INPUT size="10" type="text" name="price" value="{$lastPrice}" onKeydown="changePrice();" onBlur="itemFromPrice();">&nbsp;&nbsp;&nbsp;
    </TD>
  </TR>
  <TR>
    <TD>
      <SELECT name="itemId" onChange="changeItem();">
      {html_options selected="$itemIdSelected" values="$itemIdValues" output="$itemIdOutput"}
      </SELECT>
      &nbsp;&nbsp;
      <SELECT name="expiryDate">
      {html_options selected="$expiryDateSelected" values="$expiryDateValues" output="$expiryDateOutput"}
      </SELECT>
      &nbsp;&nbsp;&nbsp;
      <SELECT name="buySell">
      {html_options selected="buySellSelected" values="$buySellValues" output="$buySellOutput"}
      </SELECT>
      </TD>
    <TD>
      Quantity : <INPUT type="text" name="qty" value="{$minQty}" onKeyDown="changeQty();">&nbsp;&nbsp;&nbsp;
    </TD>
    <TD colspan="2">
    &nbsp;
    </TD>
  </TR>
  <TR>
{if $forStand == 1}
    <TD>
      <INPUT type="radio" name="standing" value="-1"> Open Standing
      <INPUT type="radio" name="standing" value="1"> Close Standing
    </TD>
{/if}
    <TD colspan="3">
      Vendor : 
      <SELECT name="vendor">
      {html_options selected="$vendorSelected" values="$vendorValues" output="$vendorOutput"}
      </SELECT>
    </TD>
  </TR>
  <TR>
    <TD colspan="4">
      <INPUT type="submit" name="submitBtn" value="Ok !" onClick="document.form1.makeTrade.value=1;">
    <INPUT type="reset" value="Reset">&nbsp;&nbsp;&nbsp;&nbsp;
       <B>{$clientWholeName} : </B>
       Deposit : {$deposit}&nbsp;&nbsp;&nbsp;&nbsp;
       CurrentBal : {$currentBal}&nbsp;&nbsp;&nbsp;&nbsp;
       Total : {$total}&nbsp;&nbsp;&nbsp;&nbsp;
       Phone   : {$phone}&nbsp;&nbsp;&nbsp;&nbsp;
       Mobile  : {$mobile}&nbsp;
    </TD>
  </TR>
  <TR>
    <TD colspan="4"><B>Last trade : </B>{$lastTradeInfoVar}
    </TD>
  </TR>
  </TABLE>
  {$focusScript}
  </FORM>
</BODY>
</HTML>
