<HTML>
<HEAD><TITLE>Om : Client Trades !!!</TITLE>
<STYLE>
{literal}
td{font-weight: BOLD}
{/literal}
</STYLE>  
</HEAD>
<BODY>
	<A href="./index.php">Home</A>
<TABLE border="1" cellPadding="4" cellSpacing="0">
<TR>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD colspan="2" align="center">Buy</TD>
  <TD colspan="2" align="center">Sell</TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD>&nbsp;</TD>
  <TD align="center">Item</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD align="center">B-S</TD>
</TR>
{section name="sec1" loop="$trades"}
{if $trades[sec1].dispGross != 0}
  {if $trades[sec1].totBuyQty != $trades[sec1].totSellQty}
    <TR>
      <TD>{$trades[sec1].clientId} : {$trades[sec1].clientName}</TD>
      <TD align="center">{$trades[sec1].itemIdExpiry}</TD>
      <TD align="right">{$trades[sec1].totBuyQty}</TD>
      <TD align="right">{$trades[sec1].buyRash}</TD>
      <TD align="right">{$trades[sec1].totSellQty}</TD>
      <TD align="right">{$trades[sec1].sellRash}</TD>
      <TD align="right" NOWRAP>
        {math equation="totBuyQty-totSellQty" totBuyQty=$trades[sec1].totBuyQty totSellQty=$trades[sec1].totSellQty}
      </TD>
    </TR>
  {/if}
{/if}
{/section}
<TR><TD colspan="5">&nbsp;</TD></TR>
<TR>
  <TD>&nbsp;</TD>
  <TD align="center">Item</TD>
  <TD>Buy</TD>
  <TD>Rash</TD>
  <TD>Sell</TD>
  <TD>Rash</TD>
  <TD align="center">B-S</TD>
</TR>
{section name="sec2" loop="$wholeItemArr"}
<TR>
  <TD>&nbsp;</TD>
  <TD align="center" NOWRAP>{$wholeItemArr[sec2].itemIdExpiry}</TD>
    <TD align="right" NOWRAP>
      {math equation="buyQty-sellQty" buyQty=$wholeItemArr[sec2].buyQty sellQty=$wholeItemArr[sec2].sellQty}
    </TD>
  <TD align="right">{$wholeItemArr[sec2].buyQty}</TD>
  <TD align="right">{$wholeItemArr[sec2].buyRash}</TD>
  <TD align="right">{$wholeItemArr[sec2].sellQty}</TD>
  <TD align="right">{$wholeItemArr[sec2].sellRash}</TD>
</TR>
{/section}
<TR>
  <TD>&nbsp;</TD>
  <TD align="right" NOWRAP> : Total : </TD>
    <TD align="right" NOWRAP>
      {math equation="buyQty-sellQty" buyQty=$wholeBuyQty sellQty=$wholeSellQty}
    </TD>
  <TD align="right">{$wholeBuyQty}</TD>
  <TD align="right">{$wholeBuyRash}</TD>
  <TD align="right">{$wholeSellQty}</TD>
  <TD align="right">{$wholeSellRash}</TD>
</TR>
</TABLE>
</BODY>
</HTML>
