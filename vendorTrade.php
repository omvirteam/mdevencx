<?php
  include "etc/om_config.inc";
  
  $smarty = new SmartyWWW();
  
  $lastTradeInfoVar = '';
  
  if (isset($_POST['makeTrade']) && $_POST['makeTrade'] == 1)
  {
    $tradeDate  = $_POST['tradeYear']."-".$_POST['tradeMonth']."-".$_POST['tradeDay'];

    if(isset($_POST['expiryDate']) && strlen($_POST['expiryDate']) > 0)
      $expiryDate = $_POST['expiryDate'];
    else
      $expiryDate = "";
    
    $standing = isset($_POST['standing'])?$_POST['standing']:"0";
    $insertQuery  = "INSERT INTO vendortrades (standing,
                                           buySell,itemId,tradeDate,qty,price,expiryDate,vendor)
                      VALUES ('".$standing."',
                              '".$_POST['buySell']."',
                              '".$_POST['itemId']."', '".$tradeDate."',
                              '".$_POST['qty']."', '".$_POST['price']."',
                              '".$expiryDate."', '".$_POST['vendor']."'
                                   )";
    $result = mysql_query($insertQuery);
    if(!$result)
      echo mysql_error()."<BR>".$insertQuery;
  }
  
  if(isset($_POST['price']))
    $lastPrice = $_POST['price'];
  else
    $lastPrice = '';

  if(isset($_POST['buySell']))
    $buySellSelected  = $_POST['buySell'];
  else
    $buySellSelected  = "Buy";
  $buySellValues = array("Buy", "Sell");
  $buySellOutput = array("Buy", "Sell");
////    
  if(isset($_POST['tradeDay']))
    $tradeDateDisplay = $_POST['tradeYear']."-".$_POST['tradeMonth']."-".$_POST['tradeDay']."-";
  else
    $tradeDateDisplay = date("Y-m-d");

///////////////////////////////////////////////////////
  $vendor  = '';
  $selectQuery = "SELECT * FROM vendor
                  ORDER BY vendor
                 ";
  if(isset($_POST['vendor']))
    $currentVendor = $_POST['vendor'];
  else
    $currentVendor = '';
  
  $result = mysql_query($selectQuery);
  $i = 0;
  while($row = mysql_fetch_array($result))
  {
    if($currentVendor == '')
    {
      $currentVendor = $row['vendor'];
      $vendorSelected = $row['vendor'];
      $vendor     = $row['vendor'];
      $vendorWholeName = $row['vendor'];
    }
    
    if($row['vendor'] == $currentVendor)
    {
      $vendorSelected = $row['vendor'];
      $vendor     = $row['vendor'];
      $vendorWholeName = $row['vendor'];
      //////////////////////////////////////////////
      $currentBal = $row['currentBal'];
      $deposit = $row['deposit'];
      $phone   = $row['phone'];
      $mobile  = $row['mobile'];
      //////////////////////////////////////////////
    }

    $vendorValues[$i] = $row['vendor'];
    $vendorOutput[$i] = $row['vendor'];
    $i++;
  }
///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
  $minQty = 0;
  $selectItemQuery = "SELECT * FROM item ORDER BY itemId";
  if(isset($_POST['itemId']))
    $currentItemId = $_POST['itemId'];
  else
    $currentItemId = '';
  
  $itemResult = mysql_query($selectItemQuery);
  
  $i = 0;
  $itemIdSelected = '';
  $itemIdValues = array();
  $itemIdOutput = array();
  $itemFromPriceJS = '';
  while($itemRow = mysql_fetch_array($itemResult))
  {
    if($currentItemId == '')
      $currentItemId = $itemRow['itemId'];
    
    if($itemRow['itemId'] == $currentItemId)
    {
      $itemIdSelected = $itemRow['itemId'];
      $minQty = $itemRow['min'];
    }
    
    $itemIdValues[$i] = $itemRow['itemId'];
    $itemIdOutput[$i] = $itemRow['itemId'];
    
    //For JavaScript of itemFromPrice :Start
    if(!is_null($itemRow['rangeStart']) && !is_null($itemRow['rangeEnd']))
    {
      $itemFromPriceJS .= "  if(price >= ".$itemRow['rangeStart']." && price <= ".$itemRow['rangeEnd'].")\n";
      $itemFromPriceJS .= "  {\n";
      $itemFromPriceJS .= "    document.form1.itemId.selectedIndex = ".$i.";\n";
      $itemFromPriceJS .= "    changeItem();\n";
      $itemFromPriceJS .= "  }\n";
    }
    //For JavaScript of itemFromPrice :End
    $i++;
  }
/////////////////////////////////////////////
/////////////////////////////////////////////
  $expiryQuery = "SELECT * FROM expiry
                    WHERE itemId = '".$currentItemId."'";
  $expiryResult = mysql_query($expiryQuery);
  
  $i = 0;
  $expiryDateSelected = '';
  $expiryDateValues  = array();
  $expiryDateOutput  = array();
  while($expiryRow = mysql_fetch_array($expiryResult))
  {
    $expiryDateValues[$i] = $expiryRow['expiryDate'];
    $expiryDateOutput[$i] = $expiryRow['expiryDate'];
    $i++;
  }
/////////////////////////////////////////////
/////////////////////////////////////////////
  if(isset($_POST['submitBtn']))
  {
    $vendorInfoQuery  = "SELECT * FROM vendor
                              WHERE vendor = '".$_POST['vendor']."'";
    $vendorInfoResult = mysql_query($vendorInfoQuery);
    
    while($vendorInfoRow = mysql_fetch_array($vendorInfoResult))
    {
      $nameToDisplay = $vendorInfoRow['vendor'];
    }
    
    if(isset($_POST['standing']) && $_POST['standing']==1)
      $standDisplay = " * Close Standing";
    elseif(isset($_POST['standing']) && $_POST['standing']==2)
      $standDisplay = " * Open Standing";
    else
      $standDisplay = "";
    if(isset($_POST['expiryDate']) && strlen($_POST['expiryDate']) > 0)
      $expiryDate = $_POST['expiryDate'];
    else
      $expiryDate = "";
    $lastTradeInfoVar = $nameToDisplay." * Date : ".$_POST['tradeDay']."-".$_POST['tradeMonth']."-".$_POST['tradeYear']." * Price : ".$_POST['price']." * ".$_POST['itemId']." ".$expiryDate." * ".$_POST['buySell']." * Qty : ".$_POST['qty'].$standDisplay;
  }
  else
  {
    $lastTradeIdQuery  = "SELECT max(tradeId) AS maxTradeId FROM vendortrades";
    $lastTradeIdResult = mysql_query($lastTradeIdQuery);
    
    while($lastTradeIdRow = mysql_fetch_array($lastTradeIdResult))
    {
      if($lastTradeIdRow['maxTradeId'] > 0)
      {
        $lastTradeInfoQuery  = "SELECT * FROM vendortrades
                                  WHERE tradeId = ".$lastTradeIdRow['maxTradeId'];
        $lastTradeInfoResult = mysql_query($lastTradeInfoQuery);
        
        while($lastTradeInfoRow = mysql_fetch_array($lastTradeInfoResult))
        {
//          $vendorInfoQuery  = "SELECT * FROM vendor
//                                    WHERE vendor = ".$lastTradeInfoRow['vendor'];
//          $vendorInfoResult = mysql_query($vendorInfoQuery);
//          
//          while($vendorInfoRow = mysql_fetch_array($vendorInfoResult))
//          {
            $nameToDisplay = $lastTradeInfoRow['vendor'];
//          }
          
          if($lastTradeInfoRow['standing']==1)
            $standDisplay = " * Close Standing";
          elseif($lastTradeInfoRow['standing']==2)
            $standDisplay = " * Open Standing";
          else
            $standDisplay = "";
  
          $lastTradeInfoVar = $nameToDisplay." * Date : ".substr($lastTradeInfoRow['tradeDate'],8,2)."-".substr($lastTradeInfoRow['tradeDate'],5,2)."-".substr($lastTradeInfoRow['tradeDate'],2,2)." * Price : ".$lastTradeInfoRow['price']." * ".$lastTradeInfoRow['itemId']." ".$lastTradeInfoRow['expiryDate']." * ".$lastTradeInfoRow['buySell']." * Qty : ".$lastTradeInfoRow['qty'].$standDisplay;
        }
      }
    }
  }
//////////////////////////////////////////////////
//////////////////////////////////////////////////
  if(isset($_POST['changedField']) && $_POST['changedField'] == "itemId")
    $focusScript = '<SCRIPT language="javascript">document.form1.itemId.focus();</SCRIPT>';
  elseif(isset($_POST['changedField']) && $_POST['changedField'] == "vendor")
    $focusScript = '<SCRIPT language="javascript">document.form1.vendor.focus();</SCRIPT>';
  else
    $focusScript = '<SCRIPT language="javascript">document.form1.vendor.focus();</SCRIPT>';
//////////////////////////////////////////////////
  if(isset($_GET['forStand']))
    $forStand = $_GET['forStand'];
  elseif(isset($_POST['forStand']))
    $forStand = $_POST['forStand'];
  else
    $forStand = 0;
//////////////////////////////////////////////////
  $smarty->assign("itemFromPriceJS",  $itemFromPriceJS);
  $smarty->assign("PHP_SELF",         $_SERVER['PHP_SELF']);
  $smarty->assign("vendor",           $vendor);
  $smarty->assign("vendorSelected", $vendorSelected);
  $smarty->assign("vendorValues",   $vendorValues);
  $smarty->assign("vendorOutput",   $vendorOutput);
  $smarty->assign("tradeDateDisplay", $tradeDateDisplay);
  $smarty->assign("buySellSelected",  $buySellSelected);
  $smarty->assign("buySellValues",    $buySellValues);
  $smarty->assign("buySellOutput",    $buySellOutput);
  $smarty->assign("itemIdSelected",   $itemIdSelected);
  $smarty->assign("itemIdValues",     $itemIdValues);
  $smarty->assign("itemIdOutput",     $itemIdOutput);
  $smarty->assign("expiryDateSelected", $expiryDateSelected);
  $smarty->assign("expiryDateValues"  , $expiryDateValues);
  $smarty->assign("expiryDateOutput"  , $expiryDateOutput);
  $smarty->assign("lastPrice"         , $lastPrice);
  $smarty->assign("vendorWholeName"   , $vendorWholeName);
  $smarty->assign("currentBal"        , $currentBal);
  $smarty->assign("total"             , ($deposit + $currentBal));
  $smarty->assign("deposit"           , $deposit);
  $smarty->assign("phone"             , $phone);
  $smarty->assign("mobile"            , $mobile);
  $smarty->assign("minQty"            , $minQty);
  $smarty->assign("lastTradeInfoVar"  , $lastTradeInfoVar);
  $smarty->assign("focusScript"  ,      $focusScript);
  $smarty->assign("forStand"          , $forStand);
  
  $smarty->display("vendorTrade.tpl");
?>