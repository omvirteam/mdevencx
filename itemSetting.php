<?php
  include "./etc/om_config.inc";
  $smarty = new SmartyWWW();
  
  $itemValues   = array();
  $itemSelected = "";
  $itemOutput   = array();

  $minQty = 0;
  $selectItemQuery = "SELECT * FROM item";
  if(isset($_POST['itemId']))
    $currentItemId = $_POST['itemId'];
  else
    $currentItemId = '';
  
  $itemResult = mysql_query($selectItemQuery);
  $i = 0;
  while($itemRow = mysql_fetch_array($itemResult))
  {
    if($currentItemId == '')
      $currentItemId = $itemRow['itemId'];
    
//    if($itemRow['itemId'] == $currentItemId)
//    {
//      $SELECTED = "SELECTED";
//      $minQty = $itemRow['min'];
//      if($brok == 0)
//        $brok   = $itemRow['brok'];
//    }
    else
      $SELECTED = "";
    
    $itemValues[$i] = $itemRow['itemId'];
    $itemOutput[$i] = $itemRow['itemId'];
    
    $i++;
  }
  $itemSelected = $SELECTED;
  
  $expiryArray = array();
  $expiryQuery = "SELECT * FROM expiry
                    ORDER BY itemId";
  $expiryResult = mysql_query($expiryQuery);
  $i = 0;
  while($expiryRow = mysql_fetch_array($expiryResult))
  {
    $expiryArray[$i]['itemId']     = $expiryRow['itemId'];
    $expiryArray[$i]['expiryDate'] = $expiryRow['expiryDate'];
    
    $i++;
  }
  
  $smarty->assign("itemValues",   $itemValues);
  $smarty->assign("itemSelected", $itemSelected);
  $smarty->assign("itemOutput",   $itemOutput);
  $smarty->assign("expiryArray",  $expiryArray);

  $smarty->display("itemSetting.tpl");
?>